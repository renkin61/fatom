<?php

return [
    'import_directory' => env('IMPORT_DIRECTORY', './resources/data'),

    'data_structure_directory' => env('DATA_STRUCTURE_DIRECTORY', 'structure'),

    'data_structure_file' => env('DATA_STRUCTURE_FILE', 'структура_фатом_конечная.xlsx'),

    'data_items_directory' => env('DATA_ITEMS_DIRECTORY', 'products'),
];
