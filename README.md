
##Installation & configuration

---
###Configuration & initialization
See constants in a .env.example e.g. if:
```php
IMPORT_DIRECTORY=./resources/data
DATA_STRUCTURE_DIRECTORY=structure
DATA_STRUCTURE_FILE=структура_фатом_конечная.xlsx
DATA_ITEMS_DIRECTORY=products
```
then structure will be pulled from
```php
./resources/data/structure/структура_фатом_конечная.xlsx
```
products will be pulled from .xlsx files located in  
```php
./resources/data/products
```
then move file that consists structure of categories to ./resources/data/structure &
move files that consists products to ./resources/data/products

###Installation
```php
$ cd /path/to/project
$ php artisan migrate:refresh
$ php artisan db:seed
```
After that categories will be pulled.

For more information about the hierarchical structure and working with it, see here:
- [kalnoy/nestedset](https://appdividend.com/2018/04/30/nested-set-in-laravel-tutorial/).
