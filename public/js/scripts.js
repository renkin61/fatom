/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./resources/js/scripts.js ***!
  \*********************************/
var animate = false;
var wScroll;
$(document).ready(function () {
  $(".modalList li").each(function (index) {
    $(this).find("label").attr("for", $(this).find("label").attr("for") + "_modal");
    $(this).find("input").attr("id", $(this).find("input").attr("id") + "_modal");
  });
  $("#filter-price").slider({
    animate: "slow",
    range: true,
    max: "10000",
    values: [5, 5000],
    slide: function slide(event, ui) {
      $(".filter-price-min").val(ui.values[0]);
      $(".filter-price-max").val(ui.values[1]);
      $(".min-filter").text(ui.values[0] + " ₽");
      $(".max-filter").text(ui.values[1] + " ₽");
    }
  });
  $(".filter-price-min, .min-filter").text($("#filter-price").slider("values", 0) + " ₽");
  $(".filter-price-max, .max-filter").text($("#filter-price").slider("values", 1) + " ₽");
  jQuery(document).on('click', ".plus", function () {
    $(".tovar-quality input").val(+$(".tovar-quality input").val() + 1);
  });
  jQuery(document).on('click', ".minus", function () {
    if ($(".tovar-quality input").val() === "1") {
      return false;
    } else {
      $(".tovar-quality input").val(+$(".tovar-quality input").val() - 1);
    }
  });
  wscr();
  mobmenu();
  /*
  if($("*").is(".parallax")) {
  $.stellar();
  }
  */

  $("body").on("click", ".scroll", function (event) {
    elementClick = $(this).attr("href");
    destination = $(elementClick).offset().top;
    $("body,html").animate({
      scrollTop: destination
    }, 700);
    return false;
  });

  if ($("*").is("input[type='tel']")) {
    $("input[type='tel']").inputmask("+7(999)-999-99-99", {
      showMaskOnHover: true
    });
  }

  if ($("*").is(".decimal")) {
    $(".decimal").inputmask({
      'alias': 'decimal',
      'mask': "9{1,3}[.9{0,1}]"
    });
  }

  if ($("*").is(".integer")) {
    $(".integer").inputmask("9{0,10}");
  }

  $(".search .search-btn").click(function () {
    $(this).toggleClass("active");
    $(".search-form").fadeIn(100);
    return false;
  });
  $(document).mouseup(function (e) {
    var container = $(".search form");

    if (container.has(e.target).length === 0) {
      if ($(".search .search-btn").hasClass("active")) {
        $(".search .search-btn").removeClass("active");
        $(".search-form").fadeOut(100);
      }
    }
  });
  $(".out").on("click", ".mobmenu-toggle", function (event) {
    event.preventDefault();

    if (!animate) {
      animate = true;
      $(this).toggleClass("active");

      if ($(this).hasClass("active")) {
        $(".mobmenu").addClass("open");
        $("body").addClass("openmenu");
        $(".openmenu").css("padding-right", wScroll + "px");
        $(".header").css("margin-right", wScroll + "px");
        setTimeout(function () {
          animate = false;
        }, 600);
      } else {
        $(".mobmenu").removeClass("open");
        $(".openmenu").css("padding-right", "0px");
        $(".header").css("margin-right", "0px");
        $("body").removeClass("openmenu");
        setTimeout(function () {
          animate = false;
        }, 600);
      }
    }

    return false;
  });
  $(".out").on('click', '.mobmenu .dropdown-toggle', function (e) {
    $(this).parents(".dropdown").toggleClass("active");
    $(this).parents(".dropdown").find('.dropdown-menu').first().stop(true, true).animate({
      opacity: 'toggle',
      height: 'toggle'
    }, 300);
    return false;
  });

  if ($("*").is(".catalog-categories .open")) {
    $(".catalog-categories .open .category-list").show(0);
  }

  $(".catalog-categories").on("click", ".category-name a", function (event) {
    event.preventDefault();
    $(this).parents(".category").toggleClass("open");
    $(this).parents(".category").find(".category-list").slideToggle(300);
  });

  if ($("*").is(".catalog-filter .uislider")) {
    var refreshSwatch = function refreshSwatch(slider, ui) {
      slider.parents(".uislider").find(".value.min input").val(ui.values[0]);
      slider.parents(".uislider").find(".value.max input").val(ui.values[1]);
      slider.parents(".uislider").find(".ui-slider-handle-min span").text(ui.values[0]);
      slider.parents(".uislider").find(".ui-slider-handle-max span").text(ui.values[1]);
    };

    $('.catalog-filter .uislider .uislider-container').each(function () {
      var slider = $(this);
      $(this).slider({
        range: true,
        step: slider.data("step"),
        min: slider.data("min"),
        max: slider.data("max"),
        values: [slider.data("min"), slider.data("max")],
        create: function create() {
          slider.parents(".uislider").find(".ui-slider-handle-min span").text(slider.slider("values", 0));
          slider.parents(".uislider").find(".ui-slider-handle-max span").text(slider.slider("values", 1));
        },
        slide: function slide(event, ui) {
          refreshSwatch(slider, ui);
        },
        change: function change(event, ui) {
          refreshSwatch(slider, ui);
        }
      });
    });
    $(".value.min input").change(function () {
      $(this).parents(".uislider").find(".uislider-container").slider("values", 0, $(this).val());
    });
    $(".value.max input").change(function () {
      $(this).parents(".uislider").find(".uislider-container").slider("values", 1, $(this).val());
    });
  }

  if ($("*").is(".catalog-filter .open")) {
    $(".catalog-filter .open .section-content").show(0);
  }

  $(".catalog-filter").on("click", ".section-name a", function (event) {
    event.preventDefault();
    $(this).parents(".section").toggleClass("open");
    $(this).parents(".section").find(".section-content").slideToggle(300);
  });
  $(".sidebar-buttons").on("click", ".btn-categories", function (event) {
    event.preventDefault();
    $(this).toggleClass("open");
    $(".sidebar-buttons .btn-filter").removeClass("open");
    $(".catalog-filter").slideUp(300);
    $(".catalog-categories").slideToggle(300);
  });
  $(".sidebar-buttons").on("click", ".btn-filter", function (event) {
    event.preventDefault();
    $(this).toggleClass("open");
    $(".sidebar-buttons .btn-categories").removeClass("open");
    $(".catalog-categories").slideUp(300);
    $(".catalog-filter").slideToggle(300);
  });
  $("body").on("click", ".selectlink", function (event) {
    $(this).parents("form").find("#file").click();
    return false;
  });
  $('.modal').on('show.bs.modal', function () {
    $(".cookie").css("padding-right", wScroll + "px");
    $(".header").css("right", wScroll + "px");
  });
  $('.modal').on('hidden.bs.modal', function () {
    $(".cookie").css("padding-right", 0);
    $(".header").css("right", 0);
  });
  $(".out").on("click", ".catalog-products td:contains('Сделать заказ')", function (event) {
    event.preventDefault();
    var name = $("h1.title").text() + " - " + $(this).parents("tr").find("td:eq(0)").text() + ", " + $(this).parents("tr").find("td:eq(1)").text();
    $(".modal-order .product-name").text(name);
    $(".modal-order .product").val(name);
    $(".modal-order").modal('show');
  });

  if ($("*").is(".reviews")) {
    $(".reviews-carousel").slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      appendArrows: '.reviews .arrows',
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }]
    });
  }

  if ($("*").is("select")) {
    $("select").selectmenu();
    $(".ui-selectmenu-menu").addClass("mCustomScrollbar");
    $("body").on("click", ".ui-selectmenu-button", function () {
      id = "#" + $(this).prev("select").attr("id") + "-menu";
      width = $(this).innerWidth() + 2;
      $(id).css("width", width + "px");
    });
  }

  heights();
  itemHeightsAll();
});
$(window).on("load", function (e) {
  wscr();
  heights();
  itemHeightsAll();
});
$(window).resize(function () {
  setTimeout(function () {
    wscr();
    heights();
    itemHeightsAll();

    if ($("*").is(".parallax")) {
      $.stellar("destroy");
      $(".parallax").attr("style", "");
      $.stellar();
    }
  }, 600);

  if ($("*").is("select")) {
    $("select").selectmenu("close");
  }
});
$(window).scroll(function () {
  var scrolltop = $(this).scrollTop();

  if (scrolltop > 600) {
    $(".header").addClass("scrolled");
    $('.arrow-up').fadeIn();
  } else {
    $(".header").removeClass("scrolled");
    $('.arrow-up').fadeOut();
  }
});

function wscr() {
  if ($(document).height() > $(window).height()) {
    var block = $('<div>').css({
      'height': '50px',
      'width': '50px'
    }),
        indicator = $('<div>').css({
      'height': '200px'
    });
    $('body').append(block.append(indicator));
    var w1 = $('div', block).innerWidth();
    block.css('overflow-y', 'scroll');
    var w2 = $('div', block).innerWidth();
    $(block).remove();
    wScroll = w1 - w2;
  } else {
    wScroll = 0;
  }
}

function mobmenu() {
  $(".out").append('<div class="mobmenu"><div class="container"></div></div>');
  $(".mobmenu > .container").append('<div class="mobmenu-search">' + $(".navigation .search-form").html() + '</div>');
  $(".mobmenu > .container").append('<div class="mobmenu-navigation">' + $(".navigation .menu").html() + '</div>');
  $(".mobmenu .mobmenu-navigation .active").removeClass("active");
  $(".mobmenu > .container").append('<div class="mobmenu-contacts"></div>');
  $(".mobmenu .mobmenu-contacts").append($(".header .header-contacts").html());
  $("body").append('<div class="lines"><span></span><span></span><span></span><span></span><span></span></div>');
}

function heights() {
  if (!$(".mobmenu-search .text").is(":focus")) {
    $(".mobmenu .mobmenu-navigation").css("min-height", $(".mobmenu .container").height() - $(".mobmenu .mobmenu-contacts").innerHeight() - $(".mobmenu .mobmenu-search").innerHeight() - 1);
  }

  if ($("*").is(".rotate")) {
    $('.rotate').each(function () {
      $(this).css("width", $(this).parent("div").height());
    });
  }

  if ($("*").is(".mainblock")) {
    if ($(".navigation").is(':visible')) {
      /*$(".mainblock .banner").css("height", $(window).height() - $(".header").outerHeight() - $(".navigation").outerHeight());*/
    } else {
      $(".mainblock .banner").css("height", $(window).height() - $(".header").outerHeight());
    }

    $(".mainblock").css("padding-top", $(".header").outerHeight());
  } else {
    $(".out").css("padding-top", $(".header").outerHeight());
  }
}

;

function getName(control) {
  if (control.value != "") $(control).parents("form").find(".selectlink").addClass("selected");else $(control).parents("form").find(".selectlink").removeClass("selected");
}

function itemHeightsAll() {
  if ($("*").is(".pluses")) {
    itemHeights(".pluses .plus");
  }

  if ($("*").is(".articles")) {
    itemHeights(".articles .article");
  }
}

function itemHeights(item) {
  var maxHeight = 0;
  $(item).css("height", "auto");
  $(item).each(function () {
    if ($(this).height() > maxHeight) {
      maxHeight = $(this).height();
    }
  });
  $(item).height(maxHeight + 1);
}
/******/ })()
;