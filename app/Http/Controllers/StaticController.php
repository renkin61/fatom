<?php

namespace App\Http\Controllers;

use App\Traits\StaticActionTrait;
use Illuminate\Http\Request;

class StaticController extends Controller
{
    use StaticActionTrait;

    /*public function delivery()
    {
        $title = 'Купить Алюминиевая катанка 9,5 мм А5Е в Краснодаре ⭐ — ООО ФАТОМ цена от 208 руб. купить в Краснодаре с доставкой компания ООО «Фатом»';
        $description = 'В нашей компании вы сможете купить алюминиевая катанка 9,5 мм А5Е по низкой цене. Катанка из алюминия всегда в наличии на наших складах. Для оформления заказа воспользуйтесь формой заявки на нашем сайте, или позвоните по указанному номеру. Осуществляем доставку по всей России.';
        return view('delivery.index', [
            'title' => $title,
            'description' => $description
        ]);
    }*/

    public function __call($method, $parameters)
    {
        return $this->handle($method, $parameters);
    }
}
