<?php
namespace App\Helpers;

use App\Services\Transliterator;
use JetBrains\PhpStorm\ArrayShape;
use PhpOffice\PhpSpreadsheet\IOFactory;

class DataImport
{
    #[ArrayShape(['rawData' => "string[]", 'headers' => "string[]", 'data' => "string[]"])]
    public static function parse(string $path): array
    {
        $rawData = static::read($path);

        $arr['rawData'] = $rawData;
        $arr['headers'] = array_shift($rawData);
        $arr['data'] = $rawData;

        return $arr;
    }

    private static function read(string $path): array
    {
        $reader = IOFactory::createReaderForFile($path);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($path);
        return $spreadSheet->getActiveSheet()->toArray();
    }

    public static function transliterateItems($array) {
        $transliterator = app()->get(Transliterator::class);
        return array_map(function (array $item) use ($transliterator) {
            /**
             * @var Transliterator $transliterator
             */
            return $transliterator->transliterate($item);
        }, $array);
    }

    /**
     * @param $headers
     * @return string|int
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function getCategoryIndex($headers): string|int
    {
        $category = 'категория';
        $transliterator = app()->get(Transliterator::class);
        /**
         * @var Transliterator $transliterator
         */
        $category = $transliterator->transliterate($category);
        $tHeaders = static::transliterateItems($headers);
        foreach ($tHeaders as $k => $v) {
            if ($v == $category) {
                return $k;
            }
        }
    }

    /**
     * Creates a hierarchical structure from a two-dimensional array like
     * [
     *      ['item0', null, null],
     *      ['item1', null, null],
     *      [null, 'item1_1', null],
     *      [null, 'item1_2', null],
     *      [null, 'item1_3', null],
     *      ['item2', null, null],
     *      [null, 'item2_1', null],
     *      [null, 'item2_2', null],
     *      [null, null, 'item2_2_1'],
     *      [null, null, 'item2_2_2'],
     *      [null, null, 'item2_2_3'],
     *      [null, 'item3_2', null],
     *      ['item4', null, null],
     *      [null, 'item4_1', null],
     *      [null, null, 'item4_1_1'],
     *      ['item5', null, null],
     * ];
     *
     * @param array $data
     * @return array
     */
    public static function buildStructure(array $data) {
        $structure = [];
        $i = -1;
        $j = -1;
        $k = -1;
        $counter = 0;
        /**
         * @var Transliterator $transliterator
         */
        $transliterator = app()->get(Transliterator::class);

        foreach ($data as $value) {
            $counter++;
            if($value[0]) {
                $i++;
                $structure[$i]['name'] = $value[0];
                $structure[$i]['transliterated_name'] = $transliterator->transliterate($value[0]);
//                $j = -1;
//                $k = -1;
            }
            if($value[1]) {
                $j++;
                $structure[$i]['children'][$j]['name'] = $value[1];
                $structure[$i]['children'][$j]['transliterated_name'] = $transliterator->transliterate($value[1]);
//                $k = -1;
                //$arKey = array_keys($structure[$i]['children'], ['name' => $value[1]]);
            }
            if($value[2]) {
                $k++;
                $structure[$i]['children'][$j]['children'][$k]['name'] = $value[2];
                $structure[$i]['children'][$j]['children'][$k]['transliterated_name'] = $transliterator->transliterate($value[2]);
            }
        }
//        dd($structure);
        return $structure;
    }
}
