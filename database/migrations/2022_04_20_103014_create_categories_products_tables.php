<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\NestedSet;

return new class extends Migration
{
    private static array $defaults = [
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('transliterated_name')->default('');
            NestedSet::columns($table);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->charset = static::$defaults['charset'];
            $table->collation = static::$defaults['collation'];
        });

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->text('name');
            $table->text('transliterated_name');
            $table->double('price');
            $table->string('price_title');
            $table->string('image_path');
            $table->string('image_alt');
            $table->string('image_title');
            $table->unsignedBigInteger('category_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->charset = static::$defaults['charset'];
            $table->collation = static::$defaults['collation'];
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
    }
};
