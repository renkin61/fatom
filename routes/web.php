<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaticController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [StaticController::class, 'services']);

Route::get('/product', [StaticController::class, 'product']);

Route::get('/services', [StaticController::class, 'services']);
Route::get('/about', [StaticController::class, 'about']);
Route::get('/delivery', [StaticController::class, 'delivery']);
Route::get('/contacts', [StaticController::class, 'contacts']);

Route::get('/privacy', [StaticController::class, 'privacy']);

Route::get('/catalog1', [StaticController::class, 'catalog1']);
Route::get('/catalog2', [StaticController::class, 'catalog2']);
