@extends('layouts/main')

@section('title'){{ $title }}@endsection
@section('description'){{ $description }}!@endsection
@section('keywords') @endsection

@section('main_page_css_class') tovar-content page catalog @endsection

@section('breadcrumb_items')
    <li class="breadcrumb-item"><a href="">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ url('catalog') }}">Каталог</a></li>
    <li class="breadcrumb-item"><a href="{{ url('non-ferrous-metal') }}/">Цветной металлопрокат</a></li>
    <li class="breadcrumb-item"><a href="{{ url('aluminum') }}">Алюминий</a></li>
    <li class="breadcrumb-item active">Алюминиевая катанка 9,5 мм А5Е в Краснодаре</li>
@endsection

@section('content')
    <h1>Алюминиевая катанка 9,5 мм А5Е в Краснодаре</h1>
    <form class="form-horizontal ms2_form" method="post">
        <input type="hidden" name="id" value="189156">
        <div class="tovar-header">
            <div class="tovar-img">
                <img src="{{ asset('images/load/alumin_katanka_mini.png') }}"
                     onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                     alt="Алюминиевая катанка 9,5 мм А5Е"
                     title="Алюминиевая катанка 9,5 мм А5Е">
                <!--img src="" alt="Купить Алюминиевая катанка 9,5 мм А5Е в Краснодаре ⭐ — ООО ФАТОМ"-->
            </div>

            <div class="tovar-description">
                <div class="tovar-price-from">Цена:</div>
                <div class="tovar-desc-top">
                    <div class="tovar-price">
          <span>От 208 руб. <small>
    </small></span>
                    </div>
                    <div class="tovar-buy">
                        <div class="tovar-quality">

                            <div class="input-number__minus">-</div>
                            <input class="input-number__input" type="text" name="count" id="product_price"
                                   pattern="^[0-9]+$" value="1">
                            <div class="input-number__plus">+</div>

                        </div>
                        <button type="submit" class="btn-red small" name="ms2_action" value="cart/add">
                            В корзину
                        </button>
                    </div>
                    <div class="tovar-attention" style="font-weight: 300;line-height: 1.2;">
                        *Цены носят ознакомительный характер, не являются публичной офертой
                        Для получения подробной информации о характеристиках товаров, условиях поставки, их наличии и
                        стоимости,
                        пожалуйста, отправьте заявку в отдел продаж
                    </div>
                </div>
                <div class="characterist">
                    <div class="title">Характеристики
                        <div class="btn-oneClickOrder-Gl">
                            <div class="btn-oneClickOrder small"><a data-target=".quick_buy" data-toggle="modal"
                                                                    href="#">Купить в один
                                    клик</a></div>
                        </div>
                    </div>

                    <ul>
                        <li>
                            <span>Марка</span>
                            <span></span>
                            <b>
                                А5Е, А5Е </b>
                        </li>
                        <li hidden></li>
                        <li>
                            <span>Диаметр</span>
                            <span></span>
                            <b>
                                9.5, 9.5 </b>
                        </li>
                        <li hidden>1</li>
                        <li>
                            <span>Артикул</span>
                            <span></span>
                            <b>
                                1280000002, 1280000002 </b>
                        </li>
                        <li hidden>2</li>
                        <li hidden>3</li>
                        <li hidden>4</li>

                        <div id="option">
                            <a href="kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html#char"
                               class="option__help option__help--open">
                                <span></span>
                                <div class="options">Все характеристики</div>
                                <span></span>
                            </a>
                        </div>
                    </ul>
                </div>


                <div class="tovar-specification">
                    <a class="item" href="dostavka-avtotransportom.html" target="_blank">
                        <div class="icon">
                            <img src="{{ asset('images/static/delivery/icon-1.png') }}" alt="">
                        </div>

                        <div class="specification-desc">
                            <div class="title">Быстрая доставка</div>
                        </div>

                        <div class="more">Подробнее</div>
                    </a>
                    <a class="item" href="varianty-oplaty.html" target="_blank">
                        <div class="icon">
                            <img src="{{ asset('images/static/delivery/icon-2.png') }}" alt="">
                        </div>

                        <div class="specification-desc">
                            <div class="title">Удобная оплата</div>
                            <!-- <div class="text">Оплачивайте товар любым удобным<br> для вас способом.</div> -->
                        </div>

                        <div class="more">Подробнее</div>
                    </a>
                    <a class="item" href="kak-oformit-zakaz.html" target="_blank">
                        <div class="icon">
                            <img src="{{ asset('images/static/delivery/icon-3.png') }}" alt="">
                        </div>

                        <div class="specification-desc">
                            <div class="title">Оптом и в розницу</div>
                        </div>

                        <div class="more">Подробнее</div>
                    </a>


                    <a class="item" href="gidroabrazivnaya-rezka-metalla.html" target="_blank">
                        <div class="icon">
                            <img src="{{ asset('images/static/delivery/icon-4.png') }}" alt="">
                        </div>

                        <div class="specification-desc">
                            <div class="title">резка металла</div>
                            <!-- <div class="text">Резка изделия <br>по вашим размерам</div> -->
                        </div>

                        <div class="more">Подробнее</div>
                    </a>

                </div>
            </div>
        </div>

    </form>
    <div class="products-char">
        <div id="char"></div>
        <div class="tabs">
            <div class="tabs-top">
                <div class="tab-item tab-item--active" data-id="1">
                    <div class="tab-item__content">Характеристики</div>
                    <div class="tab-item__arrow"></div>
                </div>
                <div class="tab-item" data-id="2">
                    <div class="tab-item__content">Описание</div>
                    <div class="tab-item__arrow"></div>
                </div>
                <div class="tab-item" data-id="3">
                    <div class="tab-item__content">Доставка и оплата</div>
                    <div class="tab-item__arrow"></div>
                </div>
            </div>
            <div class="tabs-bottom">
                <div class="tab-content tab-content--active" data-id="1">
                    <div class="tab-item__info">
                        <div class="left">
                            Марка
                        </div>
                        <div class="right__item">
                            А5Е, А5Е
                        </div>
                    </div>
                    <div class="tab-item__info">
                        <div class="left">
                            Диаметр
                        </div>
                        <div class="right__item">
                            9.5, 9.5
                        </div>
                    </div>
                    <div class="tab-item__info">
                        <div class="left">
                            Артикул
                        </div>
                        <div class="right__item">
                            1280000002, 1280000002
                        </div>
                    </div>
                    <div class="tab-item__info">
                        <div class="left">
                            Состояние материала
                        </div>
                        <div class="right__item">
                            Обычное, Обычное
                        </div>
                    </div>
                    <div class="tab-item__info">
                        <div class="left">
                            НТД
                        </div>
                        <div class="right__item">
                            ГОСТ 13843-78, ГОСТ 13843-78
                        </div>
                    </div>

                </div>
                <div class="tab-content" data-id="2">
                    <div class="tab-item__info">
                        <div class="tab-desc">
                            Купить Алюминиевая катанка 9,5 мм А5Е в Краснодаре по выгодной цене. ООО ФАТОМ - широкий
                            сортамент металлопродукции от поставщика. Доставка, самовывоз с металлобазы. Склад в городе
                            Краснодар, скидка на доставку 15%. ☎ Звоните: 8 (861) 205-26-98 !
                        </div>
                    </div>
                </div>
                <div class="tab-content payment" data-id="3">
                    <div class="tab-item__info">
                        <div class="left">Способы оплаты</div>
                        <div class="right">
                            <div class="right__item">Самовывоз</div>
                        </div>
                    </div>
                    <div class="tab-item__info">
                        <div class="left">Способы доставки</div>
                        <div class="right">
                            <div class="right__item">Оплата наличными</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(".tab-item").on("click", function () {
            let tabId = $(this).attr("data-id");
            let tabBottom = $(".tabs").find(".tabs-bottom");
            let selected = tabBottom.find("[data-id=" + tabId + "]");

            $(".tab-item--active").removeClass("tab-item--active");
            $(this).addClass("tab-item--active");

            tabBottom.find(".tab-content--active").removeClass("tab-content--active");

            selected.addClass("tab-content--active");
        });
    </script>
    <div class="similar">
        <div class="title">Похожие предложения в категории</div>
        <div class="items-similar">
            <div class="item-similar">
                <div class="img"><a href="kupit-alyuminievaya-katanka-9-mm-ak-v-⭐-—-ooo-fatom.html"><img
                            src="{{ asset('images/static/delivery/alumin_katanka_mini.png') }}"
                            onerror="this.onerror=null; this.src='{{ asset('images/static/delivery/noimage_200x185_c5c.jpg') }}'"
                            alt="Алюминиевая катанка 9,5 мм А5Е"
                            title="Алюминиевая катанка 9,5 мм А5Е"></a></div>
                <div class="title-item">
                    <a href="kupit-alyuminievaya-katanka-9-mm-ak-v-⭐-—-ooo-fatom.html">Купить Алюминиевая катанка 9 мм
                        АК в Краснодаре ⭐ — ООО ФАТОМ</a>
                </div>
                <a href="kupit-alyuminievaya-katanka-9-mm-ak-v-⭐-—-ooo-fatom.html" class="btn-red">Подробнее ...</a>
            </div>
            <div class="item-similar">
                <div class="img"><a href="kupit-alyuminievaya-katanka-15-mm-aklp-5t1-v-⭐-—-ooo-fatom.html"><img
                            src="{{ asset('images/static/delivery/alumin_katanka_mini.png') }}"
                            onerror="this.onerror=null; this.src='{{ asset('images/static/delivery/noimage_200x185_c5c.jpg') }}'"
                            alt="Алюминиевая катанка 9,5 мм А5Е"
                            title="Алюминиевая катанка 9,5 мм А5Е"></a></div>
                <div class="title-item">
                    <a href="kupit-alyuminievaya-katanka-15-mm-aklp-5t1-v-⭐-—-ooo-fatom.html">Купить Алюминиевая катанка
                        15 мм АКЛП-5Т1 в Краснодаре ⭐ — ООО ФАТОМ</a>
                </div>
                <a href="kupit-alyuminievaya-katanka-15-mm-aklp-5t1-v-⭐-—-ooo-fatom.html" class="btn-red">Подробнее
                    ...</a>
            </div>
            <div class="item-similar">
                <div class="img"><a href="kupit-alyuminievaya-katanka-23-mm-aklp-5pt-v-⭐-—-ooo-fatom.html"><img
                            src="{{ asset('images/static/delivery/alumin_katanka_mini.png') }}"
                            onerror="this.onerror=null; this.src='{{ asset('images/static/delivery/noimage_200x185_c5c.jpg') }}'"
                            alt="Алюминиевая катанка 9,5 мм А5Е"
                            title="Алюминиевая катанка 9,5 мм А5Е"></a></div>
                <div class="title-item">
                    <a href="kupit-alyuminievaya-katanka-23-mm-aklp-5pt-v-⭐-—-ooo-fatom.html">Купить Алюминиевая катанка
                        23 мм АКЛП-5ПТ в Краснодаре ⭐ — ООО ФАТОМ</a>
                </div>
                <a href="kupit-alyuminievaya-katanka-23-mm-aklp-5pt-v-⭐-—-ooo-fatom.html" class="btn-red">Подробнее
                    ...</a>
            </div>
            <div class="item-similar">
                <div class="img"><a href="kupit-alyuminievaya-katanka-18-mm-aklp-5t1-v-⭐-—-ooo-fatom.html"><img
                            src="{{ asset('images/static/delivery/alumin_katanka_mini.png') }}"
                            onerror="this.onerror=null; this.src='{{ asset('images/static/delivery/noimage_200x185_c5c.jpg') }}'"
                            alt="Алюминиевая катанка 9,5 мм А5Е"
                            title="Алюминиевая катанка 9,5 мм А5Е"></a></div>
                <div class="title-item">
                    <a href="kupit-alyuminievaya-katanka-18-mm-aklp-5t1-v-⭐-—-ooo-fatom.html">Купить Алюминиевая катанка
                        18 мм АКЛП-5Т1 в Краснодаре ⭐ — ООО ФАТОМ</a>
                </div>
                <a href="kupit-alyuminievaya-katanka-18-mm-aklp-5t1-v-⭐-—-ooo-fatom.html" class="btn-red">Подробнее
                    ...</a>
            </div>

        </div>
    </div>
@endsection
