@extends('layouts/main')

@section('title'){{ $title }}@endsection
@section('description'){{ $description }}!@endsection
@section('keywords') @endsection

@section('breadcrumb_items')
    <li class="breadcrumb-item"><a href="/">Главная</a></li>
    <li class="{{ url('contacts') }}">Контакты</li>
@endsection

@section('main_page_css_class') page contacts @endsection
<?php $baseImagePath = asset('images/static/contacts') ?>
@section('content')
    <h1 class="title">Контакты</h1>
    <div class="contacts-positions">
        <ul>
            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/contact.jpg"
                                                       alt="компания ООО «Фатом»"></div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Краснодар</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">ул. Новороссийская, </br>д. 236/1 лит. А, оф. 103</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88612052698" class="mgo-number-kda" itemprop="telephone">8 (861)
                            205-26-98</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=ул.+Новороссийская,+236,+Краснодар,+Краснодарский+край,+Россия,+350059">Показать
                    на карте</a>
                </div>
            </li>

            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/stav.jpg" alt="компания ООО «Фатом»">
                </div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Ставрополь</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">Старомарьевское ш. 32</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88652205728" itemprop="telephone">8 (865) 220-57-28</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=Старомарьевское+ш.,+32,+Ставрополь,+Ставропольский+край,+355008">Показать
                    на карте</a>
                </div>
            </li>

            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/rnd.jpg" alt="компания ООО «Фатом»">
                </div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Ростов-на-Дону</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">Малое Зеленое Кольцо д.3</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88612052698" class="mgo-number-ros" itemprop="telephone">8 (861)
                            205-26-98</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=ул.+Малое+Зелёное+Кольцо,+3,+Янтарный,+Ростовская+обл.,+344094">Показать
                    на карте</a>
                </div>
            </li>

            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/semf.jpg" alt="компания ООО «Фатом»">
                </div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Симферополь</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">ул. Генерала Васильева д.27а</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88692777118" itemprop="telephone">8 (869) 277-71-18</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=ул.+Генерала+Васильева,+27,+Симферополь">Показать
                    на карте</a>
                </div>
            </li>

            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/sevast.jpg" alt="компания ООО «Фатом»">
                </div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Севастополь</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">Проспект Героев Сталинграда д.46А</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88692777118" itemprop="telephone">8 (869) 277-71-18</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=просп.+Героев+Сталинграда,+46,+Севастополь">Показать
                    на карте</a>
                </div>
            </li>

            <li class="contact">
                <div class="contact-option image"><img src="{{ $baseImagePath }}/sochi.jpg" alt="компания ООО «Фатом»">
                </div>
                <div class="contact-option city">
                    <div class="heading">Город</div>
                    <div class="value">Сочи</div>
                </div>
                <div class="contact-option address">
                    <div class="heading">Адрес</div>
                    <div class="value">ул. Пластуновская 52М</div>
                </div>
                <div class="contact-option phone">
                    <div class="heading">Телефон</div>
                    <div class="value"><a href="tel:88612052698" class="mgo-number-kda" itemprop="telephone">8 (861)
                            205-26-98</a></div>
                </div>
                <div class="contact-option map">
                    <a data-fancybox data-options="{" iframe" : {"css" : {"width" : "80%", "height" : "80%"}}}"
                    href="https://www.google.com/maps/search/?api=1&query=ул.+Пластунская,+52м,+Сочи,+Краснодарский+край,+354057">Показать
                    на карте</a>
                </div>
            </li>

        </ul>
    </div>
@endsection

