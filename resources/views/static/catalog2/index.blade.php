@extends('layouts/main')

@section('title'){{ $title }}@endsection
@section('description'){{ $description }}!@endsection

@section('breadcrumb_items')
    <li class="breadcrumb-item"><a href="">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ url('catalog1') }}">Каталог</a></li>
    <li class="breadcrumb-item"><a href="{{ url('non-ferrous-metal') }}">Цветной металлопрокат</a></li>
    <li class="breadcrumb-item active">Бронза</li>
@endsection

@section('main_page_css_class') page products @endsection
{!! $baseImagePath = asset('images/static/catalog2') !!}
@section('content')

<div class="products-types">
    <div class="types">
        <div class="title">
            <a href="bronzovyij-slitok/">
                Бронзовый слиток
            </a>
        </div>
        <div class="rotate">Бронзовый слиток</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-folga/">
                Бронзовая фольга
            </a>
        </div>
        <div class="rotate">Бронзовая фольга</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-chushka/">
                Бронзовая чушка
            </a>
        </div>
        <div class="rotate">Бронзовая чушка</div>
        <ul>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-brsu6n2-40-kg-gost-493-79-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрСу6Н2 40 кг ГОСТ 493-79 в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрСу6Н2 40 кг ГОСТ 493-79"
                                               title="Бронзовая чушка БрСу6Н2 40 кг ГОСТ 493-79"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-42-kg-gost-613-79-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 42 кг ГОСТ 613-79 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 42 кг ГОСТ 613-79"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 42 кг ГОСТ 613-79"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-5-kg-gost-614-97-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 5 кг ГОСТ 614-97 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 5 кг ГОСТ 614-97"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 5 кг ГОСТ 614-97"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-10-kg-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 10 кг ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 10 кг ГОСТ 18175-78"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 10 кг ГОСТ 18175-78"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-15-kg-gost-493-79-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 15 кг ГОСТ 493-79 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 15 кг ГОСТ 493-79"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 15 кг ГОСТ 493-79"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-20-kg-gost-613-79-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 20 кг ГОСТ 613-79 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 20 кг ГОСТ 613-79"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 20 кг ГОСТ 613-79"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-25-kg-gost-614-97-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 25 кг ГОСТ 614-97 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 25 кг ГОСТ 614-97"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 25 кг ГОСТ 614-97"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-chushka-bra7zh1.5s1.5-30-kg-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая чушка БрА7Ж1.5С1.5 30 кг ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-chushka-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая чушка БрА7Ж1.5С1.5 30 кг ГОСТ 18175-78"
                                               title="Бронзовая чушка БрА7Ж1.5С1.5 30 кг ГОСТ 18175-78"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-lenta/">
                Бронзовая лента
            </a>
        </div>
        <div class="rotate">Бронзовая лента</div>
        <ul>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brb2-0,02x100-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрБ2 0,02х100 мм в Краснодаре ⭐ — ООО
                            ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрБ2 0,02х100 мм"
                                               title="Бронзовая лента БрБ2 0,02х100 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brbnt1.9-0,02x200-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрБНТ1.9 0,02х200 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрБНТ1.9 0,02х200 мм"
                                               title="Бронзовая лента БрБНТ1.9 0,02х200 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brkmcz3-1-0,02x250-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрКМц3-1 0,02х250 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрКМц3-1 0,02х250 мм"
                                               title="Бронзовая лента БрКМц3-1 0,02х250 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brof6.5-0.15-0,02x300-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрОФ6.5-0.15 0,02х300 мм в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрОФ6.5-0.15 0,02х300 мм"
                                               title="Бронзовая лента БрОФ6.5-0.15 0,02х300 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brocz4-3-0,02x400-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрОЦ4-3 0,02х400 мм в Краснодаре ⭐ — ООО
                            ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрОЦ4-3 0,02х400 мм"
                                               title="Бронзовая лента БрОЦ4-3 0,02х400 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brof6-0,02x500-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БРОФ6 0,02х500 мм в Краснодаре ⭐ — ООО
                            ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БРОФ6 0,02х500 мм"
                                               title="Бронзовая лента БРОФ6 0,02х500 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-brof7-0.2-0,02x1000-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрОФ7-0.2 0,02х1000 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрОФ7-0.2 0,02х1000 мм"
                                               title="Бронзовая лента БрОФ7-0.2 0,02х1000 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-bronzovaya-lenta-broczs4-4-4-0,02x1500-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Бронзовая лента БрОЦС4-4-4 0,02х1500 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/bronzovaya-lenta-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Бронзовая лента БрОЦС4-4-4 0,02х1500 мм"
                                               title="Бронзовая лента БрОЦС4-4-4 0,02х1500 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-setka/">
                Бронзовая сетка
            </a>
        </div>
        <div class="rotate">Бронзовая сетка</div>
        <ul>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0.4-0,04x0,03-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БрОФ6.5-0.4 0,04х0,03 мм в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БрОФ6.5-0.4 0,04х0,03 мм"
                                               title="Сетка бронзовая БрОФ6.5-0.4 0,04х0,03 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0,04x0,03-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БРОФ6.5 0,04х0,03 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БРОФ6.5 0,04х0,03 мм"
                                               title="Сетка бронзовая БРОФ6.5 0,04х0,03 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0.4-0,04x0,04-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БрОФ6.5-0.4 0,04х0,04 мм в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БрОФ6.5-0.4 0,04х0,04 мм"
                                               title="Сетка бронзовая БрОФ6.5-0.4 0,04х0,04 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0,04x0,04-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БРОФ6.5 0,04х0,04 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БРОФ6.5 0,04х0,04 мм"
                                               title="Сетка бронзовая БРОФ6.5 0,04х0,04 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0.4-0,04x0,05-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БрОФ6.5-0.4 0,04х0,05 мм в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БрОФ6.5-0.4 0,04х0,05 мм"
                                               title="Сетка бронзовая БрОФ6.5-0.4 0,04х0,05 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0,04x0,05-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БРОФ6.5 0,04х0,05 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БРОФ6.5 0,04х0,05 мм"
                                               title="Сетка бронзовая БРОФ6.5 0,04х0,05 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0.4-0,04x0,06-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БрОФ6.5-0.4 0,04х0,06 мм в Краснодаре ⭐
                            — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БрОФ6.5-0.4 0,04х0,06 мм"
                                               title="Сетка бронзовая БрОФ6.5-0.4 0,04х0,06 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-setka-bronzovaya-brof6.5-0,04x0,06-mm-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Сетка бронзовая БРОФ6.5 0,04х0,06 мм в Краснодаре ⭐ —
                            ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/setka-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Сетка бронзовая БРОФ6.5 0,04х0,06 мм"
                                               title="Сетка бронзовая БРОФ6.5 0,04х0,06 мм"></p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="plita-bronzovaya/">
                Плита бронзовая
            </a>
        </div>
        <div class="rotate">Плита бронзовая</div>
        <ul>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brazh9-10x300x500-mm-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрАЖ9 10х300х500 мм ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрАЖ9 10х300х500 мм ГОСТ 18175-78"
                                               title="Плита бронзовая БрАЖ9 10х300х500 мм ГОСТ 18175-78">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brazh9-4-10x300x1000-mm-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрАЖ9-4 10х300х1000 мм ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрАЖ9-4 10х300х1000 мм ГОСТ 18175-78"
                                               title="Плита бронзовая БрАЖ9-4 10х300х1000 мм ГОСТ 18175-78">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brof-10x300x1500-mm-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрОФ 10х300х1500 мм ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрОФ 10х300х1500 мм ГОСТ 18175-78"
                                               title="Плита бронзовая БрОФ 10х300х1500 мм ГОСТ 18175-78">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brx1-10x300x2000-mm-tu-48-21-779-85-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрХ1 10х300х2000 мм ТУ 48-21-779-85 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрХ1 10х300х2000 мм ТУ 48-21-779-85"
                                               title="Плита бронзовая БрХ1 10х300х2000 мм ТУ 48-21-779-85">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brazh-10x300x2500-mm-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрАЖ 10х300х2500 мм ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрАЖ 10х300х2500 мм ГОСТ 18175-78"
                                               title="Плита бронзовая БрАЖ 10х300х2500 мм ГОСТ 18175-78">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brx-10x300x3000-mm-tu-48-21-779-85-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрХ 10х300х3000 мм ТУ 48-21-779-85 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрХ 10х300х3000 мм ТУ 48-21-779-85"
                                               title="Плита бронзовая БрХ 10х300х3000 мм ТУ 48-21-779-85">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brnbt-10x500x500-mm-gost-18175-78-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрНБТ 10х500х500 мм ГОСТ 18175-78 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрНБТ 10х500х500 мм ГОСТ 18175-78"
                                               title="Плита бронзовая БрНБТ 10х500х500 мм ГОСТ 18175-78">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="kupit-plita-bronzovaya-brazh9-10x500x1000-mm-tu-48-21-92-89-v-⭐-—-ooo-fatom.html">
                    <div class="type-border">
                        <p class="type-name">Купить Плита бронзовая БрАЖ9 10х500х1000 мм ТУ 48-21-92-89 в
                            Краснодаре ⭐ — ООО ФАТОМ</p>
                    </div>
                    <p class="type-image"><img src="{{ $baseImagePath }}/plita-bronzovaya-kupit.png"
                                               onerror="this.onerror=null; this.src='/assets/cache_image/noimage_200x185_c5c.jpg'"
                                               alt="Плита бронзовая БрАЖ9 10х500х1000 мм ТУ 48-21-92-89"
                                               title="Плита бронзовая БрАЖ9 10х500х1000 мм ТУ 48-21-92-89">
                    </p>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="shestigrannik-bronzovyij/">
                Шестигранник бронзовый
            </a>
        </div>
        <div class="rotate">Шестигранник бронзовый</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-polosa/">
                Бронзовая полоса
            </a>
        </div>
        <div class="rotate">Бронзовая полоса</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovaya-vtulka/">
                Бронзовая втулка
            </a>
        </div>
        <div class="rotate">Бронзовая втулка</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="shina-bronzovaya/">
                Шина бронзовая
            </a>
        </div>
        <div class="rotate">Шина бронзовая</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovyij-kvadrat/">
                Бронзовый квадрат
            </a>
        </div>
        <div class="rotate">Бронзовый квадрат</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="truba-bronzovaya/">
                Труба бронзовая
            </a>
        </div>
        <div class="rotate">Труба бронзовая</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="prutok-bronzovyij/">
                Пруток бронзовый
            </a>
        </div>
        <div class="rotate">Пруток бронзовый</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovyij-list/">
                Бронзовый лист
            </a>
        </div>
        <div class="rotate">Бронзовый лист</div>
        <ul>

        </ul>
    </div>
    <div class="types">
        <div class="title">
            <a href="bronzovyij-krug/">
                Бронзовый круг
            </a>
        </div>
        <div class="rotate">Бронзовый круг</div>
        <ul>

        </ul>
    </div>
</div>
@endsection
