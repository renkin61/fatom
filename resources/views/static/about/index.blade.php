@extends('layouts/main')

@section('title'){{ $title }}@endsection
@section('description'){{ $description }}!@endsection
@section('keywords') @endsection

@section('main_page_css_class') page about @endsection

@section('breadcrumb_items')
    <li class="breadcrumb-item"><a href="/">Главная</a></li>
    <li class="breadcrumb-item active">О компании</li>
@endsection

@section('content')
<h1 class="title">О компании</h1>

<!-- BEGIN infoblock -->
<div class="infoblock">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="infoblock-text">
                <!-- BEGIN textblock -->
                <div class="textblock">
                    <p><img alt="" height="407" src="{{ asset('images/static/about/about.jpg') }}" style="float:right" width="585" /></p>

                    <p>ООО «Фатом» осуществляет свою деятельность в сфере поставок, а также обработки металлопроката и полимерных изделий. Мы рады предложить огромный выбор товаров и широкий спектр услуг по выгодным ценам.</p>

                    <h3>У нас можно приобрести</h3>

                    <ul>
                        <li>все виды проката высокого качества (сортовой, листовой, нержавеющий, а также прокат цветных металлов);</li>
                        <li>материалы ВСП;</li>
                        <li>метизную продукцию;</li>
                        <li>фитинги и трубы из полипропилена, полиэтилена и металлопластика;</li>
                        <li>трубопроводную арматуру.</li>
                    </ul>

                    <h3>Компания ООО «Фатом» осуществляет следующие виды работ</h3>

                    <ul>
                        <li>цинкование металла;</li>
                        <li>порошковое окрашивание металлических изделий;</li>
                        <li>лазерная резка металла;</li>
                        <li>плазменная резка;</li>
                        <li>рубка листового металла гильотиной;</li>
                        <li>резка металла газом;</li>
                        <li>покрытие труб и фасонных изделий изоляцией ВУС и ППУ.&nbsp;</li>
                    </ul>

                    <p>Наша компания постоянно развивается, расширяя ассортимент реализуемой продукции и повышая качество оказываемых услуг.&nbsp;</p>

                    <h3>Выгоды работы с ООО «Фатом»</h3>

                    <ul>
                        <li>мы поставляем продукцию только от известных производителей;</li>
                        <li>представляем широчайший ассортимент металлической продукции, цветного и нержавеющего проката в Краснодаре;</li>
                        <li>большой выбор промышленных изделий сопутствующего характера позволяет приобрести все необходимое в одном месте;</li>
                        <li>оперативность поставок и выполнения работ;</li>
                        <li>наши специалисты берутся за выполнение работ любой сложности;</li>
                        <li>мы реализуем индивидуальный подход к каждому заказчику;</li>
                        <li>действующая программа лояльности в сочетании с гибкой ценовой политикой нашей компании позволяет нашим клиентам экономить.</li>
                    </ul>

                    <p>Наши контактные телефоны: <strong>+7 (961) 592-86-58, 8 (861) 991-11-78.</strong></p>

                    <p>Мы будем рады сотрудничать с вами!<br />
                        &nbsp;</p>

                </div>
                <!-- END textblock -->
            </div>
        </div>
        <!--
        <div class="col-md-5 col-lg-6 col-xl-7">
            <div class="infoblock-image">
                <img src="tpl/img/about.jpg" alt="">
            </div>
        </div>
        -->
    </div>
</div>
<!-- END infoblock -->

<!-- BEGIN pluses -->
<div class="pluses">
    <div class="title">С нами выгодно работать</div>
    <div class="pluses-content">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>1</span>
                        <p>РЕАЛИЗУЕМ КРУПНЫЕ ПРОЕКТЫ</p>
                    </div>
                    <div class="plus-body">
                        Работаем с крупными российскими металлургическими заводами. Легко выполняем комплексные заказы на несколько позиций.
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>2</span>
                        <p>СЛЕДИМ ЗА АССОРТИМЕНТОМ</p>
                    </div>
                    <div class="plus-body">
                        Вы получите любой товар из каталога. Если товара нет в наличии, его можно заказать  у наших менеджеров. В кратчайшие сроки товар окажется у Вас.
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>3</span>
                        <p>БЫСТРО ДОСТАВЛЯЕМ</p>
                    </div>
                    <div class="plus-body">
                        Оперативная доставка на объект, отправка транспортными компаниями. Возможна доставка в день обращения.
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>4</span>
                        <p>ЭКОНОМИМ ВРЕМЯ</p>
                    </div>
                    <div class="plus-body">
                        Оперативный расчет заявок. Заявки принимаются в свободной форме и просчитываются в полном объеме, что избавляет наших Клиентов от утомительного поиска.
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>5</span>
                        <p>ПРОГРАММА ЛОЯЛЬНОСТИ</p>
                    </div>
                    <div class="plus-body">
                        Для постоянных клиентов предусмотрена гибкая система скидок на весь ассортимент, возможна бесплатная доставка, оплата по факту получения груза, а также отсрочка платежа до 30-ти дней.
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="plus">
                    <div class="plus-heading">
                        <span>6</span>
                        <p>СПОСОБЫ ОПЛАТЫ</p>
                    </div>
                    <div class="plus-body">
                        Наличный и безналичный расчет, возможна оплата онлайн-переводом.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END pluses -->

<!-- BEGIN video -->

<!-- END video -->
@endsection
