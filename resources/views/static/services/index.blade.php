@extends('layouts/main')

@section('title'){{ $title }}@endsection
@section('description'){{ $description }}!@endsection
@section('keywords') @endsection

@section('main_page_css_class') page products @endsection

@section('breadcrumb_items')
    <li class="breadcrumb-item"><a href="/">Главная</a></li>
    <li class="breadcrumb-item active">Услуги</li>
@endsection

<?php $baseImagePath = asset('images/static/services') ?>
@section('content')
    <!-- BEGIN services -->
    <div class="servicesOuter">
        <div class="container">
            <div class="title title-link">Услуги</div>
            <div class="services-content">
                <div class="rotate">Услуги</div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="gidroabrazivnaya-rezka-metalla.html"><img
                                        src="{{ $baseImagePath }}/gidroabrazivnaya_rezka_metalla_222x292_de9.jpg"
                                        alt="Гидроабразивная резка металла"></a>
                            </div>
                            <div class="service-text">
                                <a href="gidroabrazivnaya-rezka-metalla.html" class="name">Гидроабразивная резка
                                    металла</a>
                                <div class="description">
                                    Оказываем услуги гидроабразивной резки в Краснодаре по умеренным ценам.
                                </div>
                                <button onclick="window.location.href = 'gidroabrazivnaya-rezka-metalla.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="rezka-metalla-gazom.html"><img
                                        src="{{ $baseImagePath }}/rezka_metalla_gazom_222x292_de9.jpg"
                                        alt="Газокислородная резка металла (резка металла  газом)"></a>
                            </div>
                            <div class="service-text">
                                <a href="rezka-metalla-gazom.html" class="name">Газокислородная резка металла
                                    (резка металла газом)</a>
                                <div class="description">
                                    Недорого и качественно выполняем газокислородную резку металла в Краснодаре.
                                </div>
                                <button onclick="window.location.href = 'rezka-metalla-gazom.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="lazernaya-rezka-metalla.html"><img
                                        src="{{ $baseImagePath }}/service-1_222x292_de9.jpg"
                                        alt="Лазерная резка металла"></a>
                            </div>
                            <div class="service-text">
                                <a href="lazernaya-rezka-metalla.html" class="name">Лазерная резка металла</a>
                                <div class="description">
                                    Предоставляем услуги по лазерной резке металла в Краснодаре по выгодным
                                    ценам.
                                </div>
                                <button onclick="window.location.href = 'lazernaya-rezka-metalla.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="plazmennaya-rezka-metalla.html"><img
                                        src="{{ $baseImagePath }}/plazmennaya_rezka_metalla_222x292_de9.jpg"
                                        alt="Плазменная резка металла"></a>
                            </div>
                            <div class="service-text">
                                <a href="plazmennaya-rezka-metalla.html" class="name">Плазменная резка
                                    металла</a>
                                <div class="description">
                                    Оказываем услуги плазменной резки металла по одной из самых низких цен в
                                    Краснодаре
                                </div>
                                <button onclick="window.location.href = 'plazmennaya-rezka-metalla.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="rezka-metalla-na-lentopilnom-stanke.html"><img
                                        src="{{ $baseImagePath }}/rezka_metalla_na_lentopilnom_stanke_222x292_de9.jpg"
                                        alt="Резка ленточной пилой"></a>
                            </div>
                            <div class="service-text">
                                <a href="rezka-metalla-na-lentopilnom-stanke.html" class="name">Резка ленточной
                                    пилой</a>
                                <div class="description">
                                    Предлагаем услуги по раскрою листового металла. Один из используемых нами
                                    методов – это резка ленточной пилой.
                                </div>
                                <button onclick="window.location.href = 'rezka-metalla-na-lentopilnom-stanke.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="rubka-metalla-gilotinoj.html"><img
                                        src="{{ $baseImagePath }}/rubka_metalla_gilotinoj_222x292_de9.jpg"
                                        alt="Рубка металла гильотиной"></a>
                            </div>
                            <div class="service-text">
                                <a href="rubka-metalla-gilotinoj.html" class="name">Рубка металла гильотиной</a>
                                <div class="description">
                                    Оказываем широкий спектр услуг по обработке металлических изделий,
                                    востребована услуга по раскрою металлических листов.
                                </div>
                                <button onclick="window.location.href = 'rubka-metalla-gilotinoj.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="poroshkovoe-okrashivanie-metallicheskix-izdelij.html"><img
                                        src="{{ $baseImagePath }}/poroshkovoe_okrashivanie_metallicheskix_izdelij_222x292_de9.jpg"
                                        alt="Порошковое окрашивание металлических изделий"></a>
                            </div>
                            <div class="service-text">
                                <a href="poroshkovoe-okrashivanie-metallicheskix-izdelij.html" class="name">Порошковое
                                    окрашивание металлических изделий</a>
                                <div class="description">
                                    Гарантируем высокое качество оказания услуги порошкового окрашивания
                                    металлических изделий.
                                </div>
                                <button onclick="window.location.href = 'poroshkovoe-okrashivanie-metallicheskix-izdelij.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="pokrytie-trub-i-fasonnyx-izdelij-izolyaciej-vus-i-ppu.html"><img
                                        src="{{ $baseImagePath }}/pokryitie_trub_izolyacziej_vus_ppu_222x292_de9.jpg"
                                        alt="Покрытие труб и фасонных изделий изоляцией ВУС и ППУ"></a>
                            </div>
                            <div class="service-text">
                                <a href="pokrytie-trub-i-fasonnyx-izdelij-izolyaciej-vus-i-ppu.html"
                                   class="name">Покрытие труб и фасонных изделий изоляцией ВУС и ППУ</a>
                                <div class="description">
                                    Принимаем заказы на покрытие труб и фасонных изделий ВУС и ППУ, выполняем
                                    работы на высшем уровне и строго в заранее оговоренные с заказчиком сроки.
                                </div>
                                <button onclick="window.location.href = 'pokrytie-trub-i-fasonnyx-izdelij-izolyaciej-vus-i-ppu.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="gibka-trub.html"><img
                                        src="{{ $baseImagePath }}/gibka_trub_222x292_de9.jpg"
                                        alt="Гибка труб"></a>
                            </div>
                            <div class="service-text">
                                <a href="gibka-trub.html" class="name">Гибка труб</a>
                                <div class="description">
                                    Принимаем заказы на гибку труб, гарантируем вам профессиональное выполнение
                                    работ в сочетании с демократичной ценой.
                                </div>
                                <button onclick="window.location.href = 'gibka-trub.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="gibka-listovogo-metalla.html"><img
                                        src="{{ $baseImagePath }}/service-2_222x292_de9.jpg"
                                        alt="Гибка листового металла"></a>
                            </div>
                            <div class="service-text">
                                <a href="gibka-listovogo-metalla.html" class="name">Гибка листового металла</a>
                                <div class="description">
                                    Оказываем услуги по гибки листового металла в Краснодаре. Данный тип
                                    обработки металлических листов позволяет придавать им практически любую
                                    форму.
                                </div>
                                <button onclick="window.location.href = 'gibka-listovogo-metalla.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="razmotka-armatury-i-katanki-iz-buxty-v-prutki.html"><img
                                        src="{{ $baseImagePath }}/razmotka_armatury_222x292_de9.jpg"
                                        alt="Размотка арматуры и катанки из бухты в прутки"></a>
                            </div>
                            <div class="service-text">
                                <a href="razmotka-armatury-i-katanki-iz-buxty-v-prutki.html" class="name">Размотка
                                    арматуры и катанки из бухты в прутки</a>
                                <div class="description">

                                </div>
                                <button onclick="window.location.href = 'razmotka-armatury-i-katanki-iz-buxty-v-prutki.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service">
                            <div class="service-image">
                                <a href="cinkovanie-metalla.html"><img
                                        src="{{ $baseImagePath }}/service-3_222x292_de9.jpg"
                                        alt="Цинкование металла"></a>
                            </div>
                            <div class="service-text">
                                <a href="cinkovanie-metalla.html" class="name">Цинкование металла</a>
                                <div class="description">
                                    Оказываем услуги цинкования металла в Краснодаре по выгодной стоимости.
                                </div>
                                <button onclick="window.location.href = 'cinkovanie-metalla.html';"
                                        class="btn btn-primary">Подробнее
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END services -->
@endsection
