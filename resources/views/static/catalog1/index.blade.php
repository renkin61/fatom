<div class="products-types">
    <div class="types">
        <div class="title">Черный металлопрокат</div>
        <div class="rotate">Черный металлопрокат</div>
        <div class="subtitle"></div>
        <ul>
            <li class="type">
                <a href="listovoj-prokat/">
                    <div class="type-border">
                        <p class="type-name">Листовой прокат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_1_200x185_824.png"
                                alt="Листовой прокат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="metallicheskaya-lenta/">
                    <div class="type-border">
                        <p class="type-name">Металлическая лента</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_2_200x185_824.png"
                                alt="Металлическая лента"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="provoloka/">
                    <div class="type-border">
                        <p class="type-name">Проволока</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_3_200x185_824.png"
                                alt="Проволока"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="setka-metallicheskaya/">
                    <div class="type-border">
                        <p class="type-name">Сетка металлическая</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_4_200x185_824.png"
                                alt="Сетка металлическая"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="sortovoj-metalloprokat/">
                    <div class="type-border">
                        <p class="type-name">Сортовой металлопрокат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_5_200x185_824.png"
                                alt="Сортовой металлопрокат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="stalnoj-kanat/">
                    <div class="type-border">
                        <p class="type-name">Стальной канат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_6_200x185_824.png"
                                alt="Стальной канат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="trubnyij-prokat/">
                    <div class="type-border">
                        <p class="type-name">Трубный прокат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_7_200x185_824.png"
                                alt="Трубный прокат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="fasonnyij-prokat/">
                    <div class="type-border">
                        <p class="type-name">Фасонный прокат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_8_200x185_824.png"
                                alt="Фасонный прокат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
            <li class="type">
                <a href="chugun,-chugunnyij-prokat/">
                    <div class="type-border">
                        <p class="type-name">Чугун, чугунный прокат</p>
                    </div>
                    <div class="type__image-block">
                        <p class="type-image"><img
                                src="/assets/cache_image//home/umax/web/ooofatom.ru/public_html/Screenshot_9_200x185_824.png"
                                alt="Чугун, чугунный прокат"></p>
                    </div>
                    <div class="type-border">
                        <p class="type-order">Подробнее</p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
