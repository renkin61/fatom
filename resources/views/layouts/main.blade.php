<!-- resources/views/layouts/main.blade.php -->
<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <title>@yield('title')</title>

    <meta name="description" content="@yield('description')">

    <meta name="Keywords" content="@yield('keywords')">

    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">

    <meta name="viewport" content="initial-scale=1">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fancybox/fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mCustomScrollbar/mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/screen.css') }}" media="screen">

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('css/minishop2/default.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/msearch2/default.css') }}" type="text/css" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <script type="text/javascript">miniShop2Config = {"cssUrl":"\/assets\/components\/minishop2\/css\/web\/","jsUrl":"\/assets\/components\/minishop2\/js\/web\/","actionUrl":"\/assets\/components\/minishop2\/action.php","ctx":"web","close_all_message":"\u0437\u0430\u043a\u0440\u044b\u0442\u044c \u0432\u0441\u0435","price_format":[2,"."," "],"price_format_no_zeros":true,"weight_format":[3,"."," "],"weight_format_no_zeros":true};</script>

    <script type="text/javascript">
        if (typeof mse2Config == "undefined") {mse2Config = {"cssUrl":"\/assets\/components\/msearch2\/css\/web\/","jsUrl":"\/assets\/components\/msearch2\/js\/web\/","actionUrl":"\/assets\/components\/msearch2\/action.php"};}
        if (typeof mse2FormConfig == "undefined") {mse2FormConfig = {};}
        mse2FormConfig["c53205c27441c77af8f65ac10665e2c97a3b1818"] = {"autocomplete":"queries","queryVar":"query","minQuery":3,"pageId":404};
    </script>
    <link rel="stylesheet" href="{{ asset('css/ajaxform/default.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/cityfields/cityselect.css') }}" type="text/css" />
</head>

<body>

<!-- BEGIN out -->
<!-- Только для главной страницы к out нужно добавить index -->
<div class="out" id="out">
    <!-- BEGIN mainblock -->
    <!-- Только для главной страницы нужен контейнер mainblock для header, navigation и banner -->

    <!-- BEGIN header -->
    <header itemscope itemtype="http://schema.org/WPHeader" class="header">
        <div class="navigation">
            <div class="container">
                <div class="menu" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
                    <ul class=""><li class="first"><a href="services" >Услуги</a></li><li><a href="about" >О компании</a></li><li><a href="delivery" >Доставка</a></li><li class="last"><a href="contacts" >Контакты</a></li></ul>
                </div>
            </div>
        </div>
        <meta itemprop="headline" content="ООО Фатом">
        <meta itemprop="description"
              content="Металлопрокат оптом и в розницу в Краснодаре. Производство металлоизделий - Фатом">
        <meta itemprop="keywords"
              content="металл, производство, доставка, Краснодар, опт, розница, металлопрокат ">
        <div itemscope itemtype="http://schema.org/PostalAddress" class="container">
            <a href="#" class="mobmenu-toggle"><span></span></a>
            <div class="header-logo">
                <a itemprop="url" href="https://ooofatom.ru/">
                    <img src="{{ asset('images/logo.png') }}"
                         alt="компания ООО «Фатом»">
                </a>
            </div>
            <div class="header-contacts">
                <div class="address">
                    <div class="icon" itemprop="streetAddress">
                        <div class="cfcity">
                            <a href="#cfCity" data-toggle="modal" data-target="#cfCity">Краснодар</a>
                            <!--<div class="text-center cfcity_first">
                            <p>Ваш город<br />Краснодар?</p>
                                <div class="text-center">
                                    <a href="#cfCity" class="btn btn-primary" data-dismiss="cfcity">Да</a>
                                    <a href="#cfCity" class="btn btn-default" data-toggle="modal" data-target="#cfCity">Нет</a>
                                </div>
                        </div> -->
                        </div>
                        <!-- ул. Новороссийская, </br>д. 236/1 лит. А, оф. 103 -->
                    </div>

                </div>
                <div class="mail">
                    <p class="icon"><a href="mailto:zakaz@ooofatom.ru ">zakaz@ooofatom.ru </a></p>
                </div>

                <div class="phones">
                    <div class="phones-block">
                        <a href="tel:88612052698" class="mgo-number"
                           itemprop="telephone">8 (861) 205-26-98</a>
                    </div>
                </div>
                <div class="callback">
                    <a href="#" data-toggle="modal" data-target=".modal-callback">Заказать звонок</a>
                </div>

                <div class="catalog">
                    Каталог
                    <svg width="21" height="17" viewBox="0 0 21 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect y="1" width="5" height="2" rx="1" fill="#DE202C"/>
                        <rect y="6" width="3" height="2" rx="1" fill="#DE202C"/>
                        <rect y="11" width="5" height="2" rx="1" fill="#DE202C"/>
                        <rect x="16.4775" y="9.87946" width="6.50783" height="2" rx="1" transform="rotate(48 16.4775 9.87946)" fill="#DE202C"/>
                        <circle cx="12" cy="7" r="6" stroke="#DE202C" stroke-width="2"/>
                    </svg>
                </div>

                <div>
                    <div id="msMiniCart" class="">
                        <div class="empty">
                            <a href="korzina.html"><!--  <h5>Корзина</h5>
        Вы еще ничего не выбрали --></a>
                        </div>
                        <div class="not_empty">
                            <a href="korzina.html"><strong class="ms2_total_count">0</strong></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END header -->

    <div class="mobile-catalog">
        <ul  class="nav navbar-nav">
            <div class="submenu1">
                Каталог
                <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="17.0312" width="24" height="1.5" transform="rotate(-45 0 17.0312)" fill="#DE202C"/>
                    <rect x="17" y="18.0312" width="24" height="1.5" transform="rotate(-135 17 18.0312)" fill="#DE202C"/>
                </svg>
            </div> <li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="chernyij-metalloprokat/" >Черный металлопрокат
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Черный металлопрокат
                    </div>
                    <li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="listovoj-prokat/" >Листовой прокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Листовой прокат
                            </div>
                            <li  class="submenu3" ><a href="profnastil/">Профнастил</a></li><li  class="submenu3" ><a href="list-sudovoj/">Лист судовой</a></li><li  class="submenu3" ><a href="zhest-listovaya/">Жесть листовая</a></li><li  class="submenu3" ><a href="list-stalnoj/">Лист стальной</a></li><li  class="submenu3" ><a href="list-perforirovannyij/">Лист перфорированный</a></li><li  class="submenu3" ><a href="reshyotchatyij-nastil/">Решётчатый настил</a></li><li  class="submenu3" ><a href="list-prosechno-vyityazhnoj-pvl/">Лист просечно-вытяжной ПВЛ</a></li><li  class="submenu3" ><a href="list-riflyonyij/">Лист рифлёный</a></li><li  class="submenu3" ><a href="stalnaya-plita/">Стальная плита</a></li><li  class="submenu3" ><a href="list-oczinkovannyij/">Лист оцинкованный</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="metallicheskaya-lenta/" >Металлическая лента
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Металлическая лента
                            </div>
                            <li  class="submenu3" ><a href="lenta-perforirovannaya/">Лента перфорированная</a></li><li  class="submenu3" ><a href="lenta-oczinkovannaya/">Лента оцинкованная</a></li><li  class="submenu3" ><a href="lenta-stalnaya/">Лента стальная</a></li></ul></li><li  class="submenu2" ><a href="provoloka/">Проволока</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="setka-metallicheskaya/" >Сетка металлическая
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Сетка металлическая
                            </div>
                            <li  class="submenu3" ><a href="gabionyi/">Габионы</a></li><li  class="submenu3" ><a href="setka-dvojnogo-krucheniya/">Сетка двойного кручения</a></li><li  class="submenu3" ><a href="setka-rabicza/">Сетка Рабица</a></li><li  class="submenu3" ><a href="setka/">Сетка</a></li><li  class="submenu3" ><a href="setka-tkanaya/">Сетка тканая</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="sortovoj-metalloprokat/" >Сортовой металлопрокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Сортовой металлопрокат
                            </div>
                            <li  class="submenu3" ><a href="armatura/">Арматура</a></li><li  class="submenu3" ><a href="krug-oczinkovannyij/">Круг оцинкованный</a></li><li  class="submenu3" ><a href="prutok-oczinkovannyij/">Пруток оцинкованный</a></li><li  class="submenu3" ><a href="kovanyij-krug/">Кованый круг</a></li><li  class="submenu3" ><a href="katanka-stalnaya/">Катанка стальная</a></li><li  class="submenu3" ><a href="metallicheskij-kvadrat/">Металлический квадрат</a></li><li  class="submenu3" ><a href="polosa-oczinkovannaya/">Полоса оцинкованная</a></li><li  class="submenu3" ><a href="balka/">Балка</a></li><li  class="submenu3" ><a href="krug-stalnoj/">Круг стальной</a></li><li  class="submenu3" ><a href="fibra-stalnaya/">Фибра стальная</a></li><li  class="submenu3" ><a href="polosa-stalnaya/">Полоса стальная</a></li><li  class="submenu3" ><a href="shestigrannik-stalnoj/">Шестигранник стальной</a></li><li  class="submenu3" ><a href="kovanyij-kvadrat/">Кованый квадрат</a></li><li  class="submenu3" ><a href="legirovannaya-polosa/">Легированная полоса</a></li><li  class="submenu3" ><a href="pokovka-stalnaya/">Поковка стальная</a></li><li  class="submenu3" ><a href="kovanaya-polosa/">Кованая полоса</a></li><li  class="submenu3" ><a href="prutok-stalnoj/">Пруток стальной</a></li><li  class="submenu3" ><a href="relsyi/">Рельсы</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="stalnoj-kanat/" >Стальной канат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Стальной канат
                            </div>
                            <li  class="submenu3" ><a href="kanat-trojnoj-svivki/">Канат тройной свивки</a></li><li  class="submenu3" ><a href="kanat-stalnoj-aviaczionnyij/">Канат стальной авиационный</a></li><li  class="submenu3" ><a href="tros-dlya-talej-(kanat-dlya-telfera)/">Трос для талей (канат для тельфера)</a></li><li  class="submenu3" ><a href="kanat-dlya-burovyix-ustanovok/">Канат для буровых установок</a></li><li  class="submenu3" ><a href="kanatyi-dlya-ekskavatorov/">Канаты для экскаваторов</a></li><li  class="submenu3" ><a href="morskoj-kanat-stalnoj-(korabelnyij)/">Морской канат стальной (корабельный)</a></li><li  class="submenu3" ><a href="grozotros/">Грозотрос</a></li><li  class="submenu3" ><a href="stalnoj-kanat-(tros)/">Стальной канат (трос)</a></li><li  class="submenu3" ><a href="kanat-stalnoj-dvojnoj-svivki/">Канат стальной двойной свивки</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="trubnyij-prokat/" >Трубный прокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Трубный прокат
                            </div>
                            <li  class="submenu3" ><a href="predizolirovannaya-truba/">Предизолированная труба</a></li><li  class="submenu3" ><a href="truba-dlya-nefteprovoda/">Труба для нефтепровода</a></li><li  class="submenu3" ><a href="truba-besshovnaya/">Труба бесшовная</a></li><li  class="submenu3" ><a href="truba-elektrosvarnaya/">Труба электросварная</a></li><li  class="submenu3" ><a href="truba-kotelnaya/">Труба котельная</a></li><li  class="submenu3" ><a href="asbestoczementnaya-truba/">Асбестоцементная труба</a></li><li  class="submenu3" ><a href="truba-bu/">Труба БУ</a></li><li  class="submenu3" ><a href="truba-profilnaya/">Труба профильная</a></li><li  class="submenu3" ><a href="truba-krekingovaya/">Труба крекинговая</a></li><li  class="submenu3" ><a href="truba-betonnaya/">Труба бетонная</a></li><li  class="submenu3" ><a href="truba-oczinkovannaya/">Труба оцинкованная</a></li><li  class="submenu3" ><a href="kolonkovaya-truba/">Колонковая труба</a></li><li  class="submenu3" ><a href="truba-vosstanovlennaya/">Труба восстановленная</a></li><li  class="submenu3" ><a href="ovalnaya-truba/">Овальная труба</a></li><li  class="submenu3" ><a href="truba-gazliftnaya/">Труба газлифтная</a></li><li  class="submenu3" ><a href="truba-vgp/">Труба ВГП</a></li><li  class="submenu3" ><a href="truba-magistralnaya/">Труба магистральная</a></li><li  class="submenu3" ><a href="truba-burilnaya/">Труба бурильная</a></li><li  class="submenu3" ><a href="truba-zhelezobetonnaya/">Труба железобетонная</a></li><li  class="submenu3" ><a href="truba-obsadnaya/">Труба обсадная</a></li><li  class="submenu3" ><a href="truba-nkt/">Труба НКТ</a></li><li  class="submenu3" ><a href="truba-stalnaya/">Труба стальная</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="fasonnyij-prokat/" >Фасонный прокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Фасонный прокат
                            </div>
                            <li  class="submenu3" ><a href="profil-stalnoj/">Профиль стальной</a></li><li  class="submenu3" ><a href="shveller/">Швеллер</a></li><li  class="submenu3" ><a href="profil-gnutyij/">Профиль гнутый</a></li><li  class="submenu3" ><a href="ugolok-metallicheskij/">Уголок металлический</a></li><li  class="submenu3" ><a href="shpunt-larsena/">Шпунт Ларсена</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="chugun,-chugunnyij-prokat/" >Чугун, чугунный прокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Чугун, чугунный прокат
                            </div>
                            <li  class="submenu3" ><a href="chugunnyie-trubyi/">Чугунные трубы</a></li><li  class="submenu3" ><a href="chugunnyij-krug/">Чугунный круг</a></li><li  class="submenu3" ><a href="chugun/">Чугун</a></li></ul></li></ul></li><li  class="submenu1 dropdown active">
                <div class="dropdown-row"><a class="dropdown-toggle" href="czvetnoj-metalloprokat/" >Цветной металлопрокат
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Цветной металлопрокат
                    </div>
                    <li  class="submenu2 dropdown active">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="alyuminij/" >Алюминий
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Алюминий
                            </div>
                            <li  class="submenu3" ><a href="alyuminievaya-provoloka/">Алюминиевая проволока</a></li><li  class="submenu3" ><a href="alyuminievyij-ugolok/">Алюминиевый уголок</a></li><li  class="submenu3" ><a href="alyuminievaya-shina/">Алюминиевая шина</a></li><li  class="submenu3" ><a href="alyuminievyij-krug-(kruglyak)/">Алюминиевый круг (кругляк)</a></li><li  class="submenu3" ><a href="alyuminievyij-list/">Алюминиевый лист</a></li><li  class="submenu3" ><a href="alyuminievaya-folga/">Алюминиевая фольга</a></li><li  class="submenu3" ><a href="kvadrat-alyuminievyij/">Квадрат алюминиевый</a></li><li  class="submenu3" ><a href="rulon-alyuminievyij/">Рулон алюминиевый</a></li><li  class="submenu3" ><a href="alyuminievaya-truba/">Алюминиевая труба</a></li><li  class="submenu3 active" ><a href="alyuminievaya-katanka/">Алюминиевая катанка</a></li><li  class="submenu3" ><a href="alyuminievyij-poroshok/">Алюминиевый порошок</a></li><li  class="submenu3" ><a href="alyuminievaya-profilnaya-truba/">Алюминиевая профильная труба</a></li><li  class="submenu3" ><a href="alyuminievyij-shveller/">Алюминиевый швеллер</a></li><li  class="submenu3" ><a href="alyuminievyij-profil/">Алюминиевый профиль</a></li><li  class="submenu3" ><a href="alyuminievaya-chushka-(brusok)/">Алюминиевая чушка (брусок)</a></li><li  class="submenu3" ><a href="alyuminievaya-vtulka/">Алюминиевая втулка</a></li><li  class="submenu3" ><a href="perforirovannyij-alyuminievyij-list/">Перфорированный алюминиевый лист</a></li><li  class="submenu3" ><a href="alyuminievyij-boks/">Алюминиевый бокс</a></li><li  class="submenu3" ><a href="alyuminievaya-uplotnitelnaya-prokladka/">Алюминиевая уплотнительная прокладка</a></li><li  class="submenu3" ><a href="alyuminievyij-dvutavr/">Алюминиевый двутавр</a></li><li  class="submenu3" ><a href="alyuminievaya-plita/">Алюминиевая плита</a></li><li  class="submenu3" ><a href="alyuminievaya-polosa/">Алюминиевая полоса</a></li><li  class="submenu3" ><a href="alyuminievyij-shestigrannik/">Алюминиевый шестигранник</a></li><li  class="submenu3" ><a href="alyuminievaya-setka/">Алюминиевая сетка</a></li><li  class="submenu3" ><a href="alyuminievyij-prutok/">Алюминиевый пруток</a></li><li  class="submenu3" ><a href="alyuminievaya-lenta/">Алюминиевая лента</a></li><li  class="submenu3" ><a href="riflyonyij-alyuminievyij-list/">Рифлёный алюминиевый лист</a></li><li  class="submenu3" ><a href="alyuminievyij-tavr/">Алюминиевый тавр</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="bronza/" >Бронза
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Бронза
                            </div>
                            <li  class="submenu3" ><a href="bronzovyij-slitok/">Бронзовый слиток</a></li><li  class="submenu3" ><a href="bronzovaya-folga/">Бронзовая фольга</a></li><li  class="submenu3" ><a href="bronzovaya-chushka/">Бронзовая чушка</a></li><li  class="submenu3" ><a href="bronzovaya-lenta/">Бронзовая лента</a></li><li  class="submenu3" ><a href="bronzovaya-setka/">Бронзовая сетка</a></li><li  class="submenu3" ><a href="plita-bronzovaya/">Плита бронзовая</a></li><li  class="submenu3" ><a href="shestigrannik-bronzovyij/">Шестигранник бронзовый</a></li><li  class="submenu3" ><a href="bronzovaya-polosa/">Бронзовая полоса</a></li><li  class="submenu3" ><a href="bronzovaya-vtulka/">Бронзовая втулка</a></li><li  class="submenu3" ><a href="shina-bronzovaya/">Шина бронзовая</a></li><li  class="submenu3" ><a href="bronzovyij-kvadrat/">Бронзовый квадрат</a></li><li  class="submenu3" ><a href="truba-bronzovaya/">Труба бронзовая</a></li><li  class="submenu3" ><a href="prutok-bronzovyij/">Пруток бронзовый</a></li><li  class="submenu3" ><a href="bronzovyij-list/">Бронзовый лист</a></li><li  class="submenu3" ><a href="bronzovyij-krug/">Бронзовый круг</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="dyural/" >Дюраль
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Дюраль
                            </div>
                            <li  class="submenu3" ><a href="dyuralevyij-prutok/">Дюралевый пруток</a></li><li  class="submenu3" ><a href="dyuralevyij-ugolok/">Дюралевый уголок</a></li><li  class="submenu3" ><a href="dyuralevaya-plita/">Дюралевая плита</a></li><li  class="submenu3" ><a href="krug-dyuralevyij/">Круг дюралевый</a></li><li  class="submenu3" ><a href="dyuralevaya-polosa/">Дюралевая полоса</a></li><li  class="submenu3" ><a href="dyuralevaya-truba/">Дюралевая труба</a></li><li  class="submenu3" ><a href="dyuralevyij-kvadrat/">Дюралевый квадрат</a></li><li  class="submenu3" ><a href="dyuralevyij-shveller/">Дюралевый швеллер</a></li><li  class="submenu3" ><a href="dyuralevaya-zagotovka-(bolvanka)/">Дюралевая заготовка (болванка)</a></li><li  class="submenu3" ><a href="dyuralevyij-riflyonyij-list/">Дюралевый рифлёный лист</a></li><li  class="submenu3" ><a href="dyuralevaya-lenta/">Дюралевая лента</a></li><li  class="submenu3" ><a href="dyuralevyij-shestigrannik/">Дюралевый шестигранник</a></li><li  class="submenu3" ><a href="dyuralevaya-provoloka/">Дюралевая проволока</a></li><li  class="submenu3" ><a href="dyuralevyij-list/">Дюралевый лист</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="latun/" >Латунь
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Латунь
                            </div>
                            <li  class="submenu3" ><a href="krug-latunnyij/">Круг латунный</a></li><li  class="submenu3" ><a href="latunnaya-chushka/">Латунная чушка</a></li><li  class="submenu3" ><a href="shestigrannik-latunnyij/">Шестигранник латунный</a></li><li  class="submenu3" ><a href="latunnaya-plastina/">Латунная пластина</a></li><li  class="submenu3" ><a href="latunnyij-ugolok/">Латунный уголок</a></li><li  class="submenu3" ><a href="latunnaya-vtulka/">Латунная втулка</a></li><li  class="submenu3" ><a href="lenta-latunnaya/">Лента латунная</a></li><li  class="submenu3" ><a href="folga-latunnaya/">Фольга латунная</a></li><li  class="submenu3" ><a href="truba-latunnaya/">Труба латунная</a></li><li  class="submenu3" ><a href="plita-latunnaya/">Плита латунная</a></li><li  class="submenu3" ><a href="latunnaya-profilnaya-truba/">Латунная профильная труба</a></li><li  class="submenu3" ><a href="prutok-latunnyij/">Пруток латунный</a></li><li  class="submenu3" ><a href="shina-latunnaya/">Шина латунная</a></li><li  class="submenu3" ><a href="latunnaya-trubka/">Латунная трубка</a></li><li  class="submenu3" ><a href="latunnyij-list/">Латунный лист</a></li><li  class="submenu3" ><a href="latunnaya-provoloka/">Латунная проволока</a></li><li  class="submenu3" ><a href="setka-latunnaya/">Сетка латунная</a></li><li  class="submenu3" ><a href="latunnyij-kvadrat/">Латунный квадрат</a></li><li  class="submenu3" ><a href="latunnyij-profil/">Латунный профиль</a></li><li  class="submenu3" ><a href="latunnaya-polosa/">Латунная полоса</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="med/" >Медь
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Медь
                            </div>
                            <li  class="submenu3" ><a href="mednaya-provoloka/">Медная проволока</a></li><li  class="submenu3" ><a href="mednaya-folga/">Медная фольга</a></li><li  class="submenu3" ><a href="mednyij-list/">Медный лист</a></li><li  class="submenu3" ><a href="shina-mednaya/">Шина медная</a></li><li  class="submenu3" ><a href="mednaya-vtulka/">Медная втулка</a></li><li  class="submenu3" ><a href="mednyij-kvadrat/">Медный квадрат</a></li><li  class="submenu3" ><a href="mednaya-setka/">Медная сетка</a></li><li  class="submenu3" ><a href="mednaya-plastina/">Медная пластина</a></li><li  class="submenu3" ><a href="mednyij-slitok/">Медный слиток</a></li><li  class="submenu3" ><a href="mednaya-katanka/">Медная катанка</a></li><li  class="submenu3" ><a href="polosa-mednaya/">Полоса медная</a></li><li  class="submenu3" ><a href="mednyij-prutok/">Медный пруток</a></li><li  class="submenu3" ><a href="mednaya-lenta/">Медная лента</a></li><li  class="submenu3" ><a href="mednaya-plita/">Медная плита</a></li><li  class="submenu3" ><a href="mednaya-truba/">Медная труба</a></li><li  class="submenu3" ><a href="mednyij-poroshok/">Медный порошок</a></li><li  class="submenu3" ><a href="mednaya-chushka/">Медная чушка</a></li><li  class="submenu3" ><a href="mednyij-krug/">Медный круг</a></li><li  class="submenu3" ><a href="mednaya-dyujmovaya-truba/">Медная дюймовая труба</a></li><li  class="submenu3" ><a href="shestigrannik-mednyij/">Шестигранник медный</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="svinczovyij-prokat/" >Свинцовый прокат
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Свинцовый прокат
                            </div>
                            <li  class="submenu3" ><a href="svinczovyij-list/">Свинцовый лист</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="titan/" >Титан
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Титан
                            </div>
                            <li  class="submenu3" ><a href="titanovyij-prutok/">Титановый пруток</a></li><li  class="submenu3" ><a href="titanovaya-provoloka/">Титановая проволока</a></li><li  class="submenu3" ><a href="titanovyij-krug/">Титановый круг</a></li><li  class="submenu3" ><a href="titanovyij-list/">Титановый лист</a></li><li  class="submenu3" ><a href="titanovaya-plita/">Титановая плита</a></li><li  class="submenu3" ><a href="titanovyij-kvadrat/">Титановый квадрат</a></li><li  class="submenu3" ><a href="titanovaya-polosa/">Титановая полоса</a></li></ul></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="truboprovodnaya-armatura/" >Трубопроводная арматура
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Трубопроводная арматура
                    </div>
                    <li  class="submenu2" ><a href="gidrant-pozharnyij-podzemnyij/">Гидрант пожарный подземный</a></li><li  class="submenu2" ><a href="klapan-predoxranitelnyij/">Клапан предохранительный</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="klapanyi/" >Клапаны
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Клапаны
                            </div>
                            <li  class="submenu3" ><a href="klapan-otsechnoj/">Клапан отсечной</a></li><li  class="submenu3" ><a href="klapan-zapornyij/">Клапан запорный</a></li><li  class="submenu3" ><a href="reguliruyushhij-klapan/">Регулирующий клапан</a></li><li  class="submenu3" ><a href="obratnyij-klapan/">Обратный клапан</a></li></ul></li><li  class="submenu2" ><a href="oporno-napravlyayushhee-kolczo/">Опорно направляющее кольцо</a></li><li  class="submenu2" ><a href="perexod-stalnoj/">Переход стальной</a></li><li  class="submenu2" ><a href="regulyator-davleniya/">Регулятор давления</a></li><li  class="submenu2" ><a href="termostaticheskij-klapan/">Термостатический клапан</a></li><li  class="submenu2" ><a href="truboprovodnyij-kompensator/">Трубопроводный компенсатор</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="chugunnaya-armatura/" >Чугунная арматура
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Чугунная арматура
                            </div>
                            <li  class="submenu3" ><a href="dozhdepriemnik-chugunnyij/">Дождеприемник чугунный</a></li><li  class="submenu3" ><a href="lyuk-chugunnyij/">Люк чугунный</a></li><li  class="submenu3" ><a href="zadvizhka-chugunnaya/">Задвижка чугунная</a></li><li  class="submenu3" ><a href="trap-chugunnyij/">Трап чугунный</a></li><li  class="submenu3" ><a href="chugunnoe-koleno/">Чугунное колено</a></li><li  class="submenu3" ><a href="chugunnyij-sifon/">Чугунный сифон</a></li><li  class="submenu3" ><a href="chugunnaya-krestovina/">Чугунная крестовина</a></li><li  class="submenu3" ><a href="zatvor-chugunnyij/">Затвор чугунный</a></li><li  class="submenu3" ><a href="otvod-chugunnyij/">Отвод чугунный</a></li></ul></li><li  class="submenu2" ><a href="shibernaya-zadvizhka/">Шиберная задвижка</a></li><li  class="submenu2" ><a href="shtuczer/">Штуцер</a></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="stroitelnyie-materialyi/" >Строительные материалы
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Строительные материалы
                    </div>
                    <li  class="submenu2" ><a href="doloto/">Долото</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="zhelezobetonnyie-materialyi/" >Железобетонные материалы
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Железобетонные материалы
                            </div>
                            <li  class="submenu3" ><a href="izdeliya-dlya-energeticheskoj-otrasli/">Изделия для энергетической отрасли</a></li><li  class="submenu3" ><a href="izdeliya-dlya-dorozhnogo-stroitelstva/">Изделия для дорожного строительства</a></li><li  class="submenu3" ><a href="">Изделия для инженерных сетей</a></li><li  class="submenu3" ><a href="">Изделия для промышленного и гражданского строительства</a></li></ul></li><li  class="submenu2" ><a href="kozhux-ograditelnyij/">Кожух оградительный</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="kompoziczionnyie-materialyi/" >Композиционные материалы
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Композиционные материалы
                            </div>
                            <li  class="submenu3" ><a href="kompozitnaya-armatura/">Композитная арматура</a></li></ul></li><li  class="submenu2" ><a href="metallocherepicza/">Металлочерепица</a></li><li  class="submenu2" ><a href="stroitelnyie-ograzhdeniya/">Строительные ограждения</a></li><li  class="submenu2" ><a href="sendvich-paneli/">Сэндвич-панели</a></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="speczialnyie-stali-i-splavyi/" >Специальные стали и сплавы
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Специальные стали и сплавы
                    </div>
                    <li  class="submenu2" ><a href="shestigrannik/">Шестигранник</a></li><li  class="submenu2" ><a href="ugolok/">Уголок</a></li><li  class="submenu2" ><a href="truba/">Труба</a></li><li  class="submenu2" ><a href="preczizionnyie-splavyi/">Прецизионные сплавы</a></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="svarochnyie-materialyi/" >Сварочные материалы
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Сварочные материалы
                    </div>
                    <li  class="submenu2" ><a href="elektrodyi/">Электроды</a></li><li  class="submenu2" ><a href="elektrodnaya-lenta/">Электродная лента</a></li><li  class="submenu2" ><a href="flyus/">Флюс</a></li><li  class="submenu2" ><a href="svarochnaya-provoloka/">Сварочная проволока</a></li><li  class="submenu2" ><a href="prutok-dlya-naplavki/">Пруток для наплавки</a></li><li  class="submenu2" ><a href="prisadochnyij-prutok/">Присадочный пруток</a></li><li  class="submenu2" ><a href="prisadochnaya-provoloka/">Присадочная проволока</a></li><li  class="submenu2" ><a href="pripoi/">Припои</a></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="provoda,-kabelnaya-produkcziya/" >Провода, кабельная продукция
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Провода, кабельная продукция
                    </div>
                    <li  class="submenu2" ><a href="vzryivnoj-provod/">Взрывной провод</a></li><li  class="submenu2" ><a href="kabel-kontrolnyij/">Кабель контрольный</a></li><li  class="submenu2" ><a href="kabel-liftovoj/">Кабель лифтовой</a></li><li  class="submenu2" ><a href="kabel-nagrevatelnyij/">Кабель нагревательный</a></li><li  class="submenu2" ><a href="kabel-svarochnyij/">Кабель сварочный</a></li><li  class="submenu2" ><a href="kabel-svyazi/">Кабель связи</a></li><li  class="submenu2" ><a href="kabel-signalizaczii/">Кабель сигнализации</a></li><li  class="submenu2" ><a href="kabel-silovoj-gibkij/">Кабель силовой гибкий</a></li><li  class="submenu2" ><a href="kabel-shaxtnyij/">Кабель шахтный</a></li><li  class="submenu2" ><a href="montazhnyij-provod/">Монтажный провод</a></li><li  class="submenu2" ><a href="provod-bortovoj/">Провод бортовой</a></li><li  class="submenu2" ><a href="provod-obmotochnyij/">Провод обмоточный</a></li><li  class="submenu2" ><a href="provod-termostojkij/">Провод термостойкий</a></li><li  class="submenu2" ><a href="provod-upravleniya/">Провод управления</a></li><li  class="submenu2" ><a href="silovoj-kabel/">Силовой кабель</a></li><li  class="submenu2" ><a href="sudovoj-kabel/">Судовой кабель</a></li><li  class="submenu2" ><a href="termoelektrodnyij-provod/">Термоэлектродный провод</a></li></ul></li><li  class="submenu1 dropdown">
                <div class="dropdown-row"><a class="dropdown-toggle" href="nerzhaveyushhaya-stal/" >Нержавеющая сталь
                        <span class="caret"></span>
                    </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                    <div class="submenu2">
                        <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                        </svg>
                        Нержавеющая сталь
                    </div>
                    <li  class="submenu2" ><a href="zagotovka-nerzhaveyushhaya/">Заготовка нержавеющая</a></li><li  class="submenu2" ><a href="kvadrat-nerzhaveyushhij/">Квадрат нержавеющий</a></li><li  class="submenu2" ><a href="lenta-nerzhaveyushhaya/">Лента нержавеющая</a></li><li  class="submenu2" ><a href="list-nerzhaveyushhij/">Лист нержавеющий</a></li><li  class="submenu2" ><a href="nerzhaveyushhaya-provoloka/">Нержавеющая проволока</a></li><li  class="submenu2" ><a href="nerzhaveyushhaya-truba/">Нержавеющая труба</a></li><li  class="submenu2" ><a href="nerzhaveyushhij-krug/">Нержавеющий круг</a></li><li  class="submenu2" ><a href="plita-nerzhaveyushhaya/">Плита нержавеющая</a></li><li  class="submenu2" ><a href="polosa-nerzhaveyushhaya/">Полоса нержавеющая</a></li><li  class="submenu2" ><a href="prutok-nerzhaveyushhij/">Пруток нержавеющий</a></li><li  class="submenu2" ><a href="rulon-nerzhaveyushhij/">Рулон нержавеющий</a></li><li  class="submenu2" ><a href="setka-nerzhaveyushhaya/">Сетка нержавеющая</a></li><li  class="submenu2" ><a href="tros-nerzhaveyushhij/">Трос нержавеющий</a></li><li  class="submenu2" ><a href="trubka-kapillyarnaya-nerzhaveyushhaya/">Трубка капиллярная нержавеющая</a></li><li  class="submenu2" ><a href="ugolok-nerzhaveyushhij/">Уголок нержавеющий</a></li><li  class="submenu2" ><a href="shveller-nerzhaveyushhij/">Швеллер нержавеющий</a></li><li  class="submenu2" ><a href="shestigrannik-nerzhaveyushhij/">Шестигранник нержавеющий</a></li><li  class="submenu2" ><a href="shponochnyij-prokat-nerzhaveyushhij,-shponki-nerzhaveyushhie/">Шпоночный прокат нержавеющий, шпонки нержавеющие</a></li><li  class="submenu2" ><a href="shtrips-nerzhaveyushhij/">Штрипс нержавеющий</a></li><li  class="submenu2" ><a href="metallurgicheskoe-syiryo/">Металлургическое сырьё</a></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="ferrosplavyi/" >Ферросплавы
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Ферросплавы
                            </div>
                            <li  class="submenu3" ><a href="ferromarganecz/">Ферромарганец</a></li><li  class="submenu3" ><a href="ferrovolfram/">Ферровольфрам</a></li><li  class="submenu3" ><a href="ferrotitan/">Ферротитан</a></li><li  class="submenu3" ><a href="ferromolibden/">Ферромолибден</a></li><li  class="submenu3" ><a href="ferrobor/">Ферробор</a></li><li  class="submenu3" ><a href="ferrofosfor/">Феррофосфор</a></li><li  class="submenu3" ><a href="ferrovanadij/">Феррованадий</a></li><li  class="submenu3" ><a href="ferrosiliczij/">Ферросилиций</a></li><li  class="submenu3" ><a href="ferroczerij/">Ферроцерий</a></li><li  class="submenu3" ><a href="ferrosera/">Ферросера</a></li><li  class="submenu3" ><a href="silikokalczij/">Силикокальций</a></li><li  class="submenu3" ><a href="silikomarganecz/">Силикомарганец</a></li><li  class="submenu3" ><a href="mishmetall/">Мишметалл</a></li><li  class="submenu3" ><a href="ferroniobij/">Феррониобий</a></li><li  class="submenu3" ><a href="ferrosilikomarganecz/">Ферросиликомарганец</a></li><li  class="submenu3" ><a href="ferrosilikotitan/">Ферросиликотитан</a></li><li  class="submenu3" ><a href="ferroalyuminij/">Ферроалюминий</a></li><li  class="submenu3" ><a href="ferrosilikoczirkonij/">Ферросиликоцирконий</a></li><li  class="submenu3" ><a href="ferrosilikoxrom/">Ферросиликохром</a></li><li  class="submenu3" ><a href="ferronikel/">Ферроникель</a></li><li  class="submenu3" ><a href="ferrosilikovanadij/">Ферросиликованадий</a></li><li  class="submenu3" ><a href="ferroalyumoniobij/">Ферроалюмониобий</a></li><li  class="submenu3" ><a href="ferroxrom/">Феррохром</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="izdeliya-dlya-inzhenernyix-setej/" >Изделия для инженерных сетей
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Изделия для инженерных сетей
                            </div>
                            <li  class="submenu3" ><a href="plityi-lotkov-kanalov/">Плиты лотков каналов</a></li><li  class="submenu3" ><a href="lotki-teplotrass/">Лотки теплотрасс</a></li><li  class="submenu3" ><a href="kryishki-i-plityi-perekryitiya-kolodczev/">Крышки и плиты перекрытия колодцев</a></li><li  class="submenu3" ><a href="plityi-dnishha-kolodczev/">Плиты днища колодцев</a></li><li  class="submenu3" ><a href="plityi-lotkov-teplotrass/">Плиты лотков теплотрасс</a></li><li  class="submenu3" ><a href="kolcza-kolodczev/">Кольца колодцев</a></li><li  class="submenu3" ><a href="lotki-kanalov/">Лотки каналов</a></li></ul></li><li  class="submenu2 dropdown">
                        <div class="dropdown-row"><a class="dropdown-toggle" href="izdeliya-dlya-promyishlennogo-i-grazhdanskogo-stroitelstva/" >Изделия для промышленного и гражданского строительства
                                <span class="caret"></span>
                            </a><div class="arrow-zone"><span class="dropdown-arrow"></span></div></div><ul class="dropdown-menu mobile-catalog__sub">
                            <div class="submenu2">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.305588 7.79289C-0.0849361 8.18342 -0.0849361 8.81658 0.305588 9.20711L6.66955 15.5711C7.06007 15.9616 7.69324 15.9616 8.08376 15.5711C8.47429 15.1805 8.47429 14.5474 8.08376 14.1569L2.42691 8.5L8.08376 2.84315C8.47429 2.45262 8.47429 1.81946 8.08376 1.42893C7.69324 1.03841 7.06007 1.03841 6.66955 1.42893L0.305588 7.79289ZM21.0127 7.5L1.0127 7.5V9.5L21.0127 9.5V7.5Z" fill="#DE202C"/>
                                </svg>
                                Изделия для промышленного и гражданского строительства
                            </div>
                            <li  class="submenu3" ><a href="plityi-pokryitij/">Плиты покрытий</a></li></ul></li></ul></li></ul>
    </div>

    <!-- BEGIN navigation -->
    <div class="navigation">
        <div class="container">
            <div class="menu submenu" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
                <ul  class="nav navbar-nav"><li  class="submenu1 dropdown active">
                        <a class="dropdown-toggle" href="katalog-produkcii/" >Каталог
                            <span class="caret"></span>
                        </a><ul class="dropdown-menu"><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="chernyij-metalloprokat/" >Черный металлопрокат
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="listovoj-prokat/" >Листовой прокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="profnastil/">Профнастил</a></li><li  class="submenu4" ><a href="list-sudovoj/">Лист судовой</a></li><li  class="submenu4" ><a href="zhest-listovaya/">Жесть листовая</a></li><li  class="submenu4" ><a href="list-stalnoj/">Лист стальной</a></li><li  class="submenu4" ><a href="list-perforirovannyij/">Лист перфорированный</a></li><li  class="submenu4" ><a href="reshyotchatyij-nastil/">Решётчатый настил</a></li><li  class="submenu4" ><a href="list-prosechno-vyityazhnoj-pvl/">Лист просечно-вытяжной ПВЛ</a></li><li  class="submenu4" ><a href="list-riflyonyij/">Лист рифлёный</a></li><li  class="submenu4" ><a href="stalnaya-plita/">Стальная плита</a></li><li  class="submenu4" ><a href="list-oczinkovannyij/">Лист оцинкованный</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="metallicheskaya-lenta/" >Металлическая лента
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="lenta-perforirovannaya/">Лента перфорированная</a></li><li  class="submenu4" ><a href="lenta-oczinkovannaya/">Лента оцинкованная</a></li><li  class="submenu4" ><a href="lenta-stalnaya/">Лента стальная</a></li></ul></li><li  class="submenu3" ><a href="provoloka/">Проволока</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="setka-metallicheskaya/" >Сетка металлическая
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="gabionyi/">Габионы</a></li><li  class="submenu4" ><a href="setka-dvojnogo-krucheniya/">Сетка двойного кручения</a></li><li  class="submenu4" ><a href="setka-rabicza/">Сетка Рабица</a></li><li  class="submenu4" ><a href="setka/">Сетка</a></li><li  class="submenu4" ><a href="setka-tkanaya/">Сетка тканая</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="sortovoj-metalloprokat/" >Сортовой металлопрокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="armatura/">Арматура</a></li><li  class="submenu4" ><a href="krug-oczinkovannyij/">Круг оцинкованный</a></li><li  class="submenu4" ><a href="prutok-oczinkovannyij/">Пруток оцинкованный</a></li><li  class="submenu4" ><a href="kovanyij-krug/">Кованый круг</a></li><li  class="submenu4" ><a href="katanka-stalnaya/">Катанка стальная</a></li><li  class="submenu4" ><a href="metallicheskij-kvadrat/">Металлический квадрат</a></li><li  class="submenu4" ><a href="polosa-oczinkovannaya/">Полоса оцинкованная</a></li><li  class="submenu4" ><a href="balka/">Балка</a></li><li  class="submenu4" ><a href="krug-stalnoj/">Круг стальной</a></li><li  class="submenu4" ><a href="fibra-stalnaya/">Фибра стальная</a></li><li  class="submenu4" ><a href="polosa-stalnaya/">Полоса стальная</a></li><li  class="submenu4" ><a href="shestigrannik-stalnoj/">Шестигранник стальной</a></li><li  class="submenu4" ><a href="kovanyij-kvadrat/">Кованый квадрат</a></li><li  class="submenu4" ><a href="legirovannaya-polosa/">Легированная полоса</a></li><li  class="submenu4" ><a href="pokovka-stalnaya/">Поковка стальная</a></li><li  class="submenu4" ><a href="kovanaya-polosa/">Кованая полоса</a></li><li  class="submenu4" ><a href="prutok-stalnoj/">Пруток стальной</a></li><li  class="submenu4" ><a href="relsyi/">Рельсы</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="stalnoj-kanat/" >Стальной канат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="kanat-trojnoj-svivki/">Канат тройной свивки</a></li><li  class="submenu4" ><a href="kanat-stalnoj-aviaczionnyij/">Канат стальной авиационный</a></li><li  class="submenu4" ><a href="tros-dlya-talej-(kanat-dlya-telfera)/">Трос для талей (канат для тельфера)</a></li><li  class="submenu4" ><a href="kanat-dlya-burovyix-ustanovok/">Канат для буровых установок</a></li><li  class="submenu4" ><a href="kanatyi-dlya-ekskavatorov/">Канаты для экскаваторов</a></li><li  class="submenu4" ><a href="morskoj-kanat-stalnoj-(korabelnyij)/">Морской канат стальной (корабельный)</a></li><li  class="submenu4" ><a href="grozotros/">Грозотрос</a></li><li  class="submenu4" ><a href="stalnoj-kanat-(tros)/">Стальной канат (трос)</a></li><li  class="submenu4" ><a href="kanat-stalnoj-dvojnoj-svivki/">Канат стальной двойной свивки</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="trubnyij-prokat/" >Трубный прокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="predizolirovannaya-truba/">Предизолированная труба</a></li><li  class="submenu4" ><a href="truba-dlya-nefteprovoda/">Труба для нефтепровода</a></li><li  class="submenu4" ><a href="truba-besshovnaya/">Труба бесшовная</a></li><li  class="submenu4" ><a href="truba-elektrosvarnaya/">Труба электросварная</a></li><li  class="submenu4" ><a href="truba-kotelnaya/">Труба котельная</a></li><li  class="submenu4" ><a href="asbestoczementnaya-truba/">Асбестоцементная труба</a></li><li  class="submenu4" ><a href="truba-bu/">Труба БУ</a></li><li  class="submenu4" ><a href="truba-profilnaya/">Труба профильная</a></li><li  class="submenu4" ><a href="truba-krekingovaya/">Труба крекинговая</a></li><li  class="submenu4" ><a href="truba-betonnaya/">Труба бетонная</a></li><li  class="submenu4" ><a href="truba-oczinkovannaya/">Труба оцинкованная</a></li><li  class="submenu4" ><a href="kolonkovaya-truba/">Колонковая труба</a></li><li  class="submenu4" ><a href="truba-vosstanovlennaya/">Труба восстановленная</a></li><li  class="submenu4" ><a href="ovalnaya-truba/">Овальная труба</a></li><li  class="submenu4" ><a href="truba-gazliftnaya/">Труба газлифтная</a></li><li  class="submenu4" ><a href="truba-vgp/">Труба ВГП</a></li><li  class="submenu4" ><a href="truba-magistralnaya/">Труба магистральная</a></li><li  class="submenu4" ><a href="truba-burilnaya/">Труба бурильная</a></li><li  class="submenu4" ><a href="truba-zhelezobetonnaya/">Труба железобетонная</a></li><li  class="submenu4" ><a href="truba-obsadnaya/">Труба обсадная</a></li><li  class="submenu4" ><a href="truba-nkt/">Труба НКТ</a></li><li  class="submenu4" ><a href="truba-stalnaya/">Труба стальная</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="fasonnyij-prokat/" >Фасонный прокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="profil-stalnoj/">Профиль стальной</a></li><li  class="submenu4" ><a href="shveller/">Швеллер</a></li><li  class="submenu4" ><a href="profil-gnutyij/">Профиль гнутый</a></li><li  class="submenu4" ><a href="ugolok-metallicheskij/">Уголок металлический</a></li><li  class="submenu4" ><a href="shpunt-larsena/">Шпунт Ларсена</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="chugun,-chugunnyij-prokat/" >Чугун, чугунный прокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="chugunnyie-trubyi/">Чугунные трубы</a></li><li  class="submenu4" ><a href="chugunnyij-krug/">Чугунный круг</a></li><li  class="submenu4" ><a href="chugun/">Чугун</a></li></ul></li></ul></li><li  class="submenu2 dropdown active">
                                <a class="dropdown-toggle" href="czvetnoj-metalloprokat/" >Цветной металлопрокат
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3 dropdown active">
                                        <a class="dropdown-toggle" href="alyuminij/" >Алюминий
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="alyuminievaya-provoloka/">Алюминиевая проволока</a></li><li  class="submenu4" ><a href="alyuminievyij-ugolok/">Алюминиевый уголок</a></li><li  class="submenu4" ><a href="alyuminievaya-shina/">Алюминиевая шина</a></li><li  class="submenu4" ><a href="alyuminievyij-krug-(kruglyak)/">Алюминиевый круг (кругляк)</a></li><li  class="submenu4" ><a href="alyuminievyij-list/">Алюминиевый лист</a></li><li  class="submenu4" ><a href="alyuminievaya-folga/">Алюминиевая фольга</a></li><li  class="submenu4" ><a href="kvadrat-alyuminievyij/">Квадрат алюминиевый</a></li><li  class="submenu4" ><a href="rulon-alyuminievyij/">Рулон алюминиевый</a></li><li  class="submenu4" ><a href="alyuminievaya-truba/">Алюминиевая труба</a></li><li  class="submenu4 active" ><a href="alyuminievaya-katanka/">Алюминиевая катанка</a></li><li  class="submenu4" ><a href="alyuminievyij-poroshok/">Алюминиевый порошок</a></li><li  class="submenu4" ><a href="alyuminievaya-profilnaya-truba/">Алюминиевая профильная труба</a></li><li  class="submenu4" ><a href="alyuminievyij-shveller/">Алюминиевый швеллер</a></li><li  class="submenu4" ><a href="alyuminievyij-profil/">Алюминиевый профиль</a></li><li  class="submenu4" ><a href="alyuminievaya-chushka-(brusok)/">Алюминиевая чушка (брусок)</a></li><li  class="submenu4" ><a href="alyuminievaya-vtulka/">Алюминиевая втулка</a></li><li  class="submenu4" ><a href="perforirovannyij-alyuminievyij-list/">Перфорированный алюминиевый лист</a></li><li  class="submenu4" ><a href="alyuminievyij-boks/">Алюминиевый бокс</a></li><li  class="submenu4" ><a href="alyuminievaya-uplotnitelnaya-prokladka/">Алюминиевая уплотнительная прокладка</a></li><li  class="submenu4" ><a href="alyuminievyij-dvutavr/">Алюминиевый двутавр</a></li><li  class="submenu4" ><a href="alyuminievaya-plita/">Алюминиевая плита</a></li><li  class="submenu4" ><a href="alyuminievaya-polosa/">Алюминиевая полоса</a></li><li  class="submenu4" ><a href="alyuminievyij-shestigrannik/">Алюминиевый шестигранник</a></li><li  class="submenu4" ><a href="alyuminievaya-setka/">Алюминиевая сетка</a></li><li  class="submenu4" ><a href="alyuminievyij-prutok/">Алюминиевый пруток</a></li><li  class="submenu4" ><a href="alyuminievaya-lenta/">Алюминиевая лента</a></li><li  class="submenu4" ><a href="riflyonyij-alyuminievyij-list/">Рифлёный алюминиевый лист</a></li><li  class="submenu4" ><a href="alyuminievyij-tavr/">Алюминиевый тавр</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="bronza/" >Бронза
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="bronzovyij-slitok/">Бронзовый слиток</a></li><li  class="submenu4" ><a href="bronzovaya-folga/">Бронзовая фольга</a></li><li  class="submenu4" ><a href="bronzovaya-chushka/">Бронзовая чушка</a></li><li  class="submenu4" ><a href="bronzovaya-lenta/">Бронзовая лента</a></li><li  class="submenu4" ><a href="bronzovaya-setka/">Бронзовая сетка</a></li><li  class="submenu4" ><a href="plita-bronzovaya/">Плита бронзовая</a></li><li  class="submenu4" ><a href="shestigrannik-bronzovyij/">Шестигранник бронзовый</a></li><li  class="submenu4" ><a href="bronzovaya-polosa/">Бронзовая полоса</a></li><li  class="submenu4" ><a href="bronzovaya-vtulka/">Бронзовая втулка</a></li><li  class="submenu4" ><a href="shina-bronzovaya/">Шина бронзовая</a></li><li  class="submenu4" ><a href="bronzovyij-kvadrat/">Бронзовый квадрат</a></li><li  class="submenu4" ><a href="truba-bronzovaya/">Труба бронзовая</a></li><li  class="submenu4" ><a href="prutok-bronzovyij/">Пруток бронзовый</a></li><li  class="submenu4" ><a href="bronzovyij-list/">Бронзовый лист</a></li><li  class="submenu4" ><a href="bronzovyij-krug/">Бронзовый круг</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="dyural/" >Дюраль
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="dyuralevyij-prutok/">Дюралевый пруток</a></li><li  class="submenu4" ><a href="dyuralevyij-ugolok/">Дюралевый уголок</a></li><li  class="submenu4" ><a href="dyuralevaya-plita/">Дюралевая плита</a></li><li  class="submenu4" ><a href="krug-dyuralevyij/">Круг дюралевый</a></li><li  class="submenu4" ><a href="dyuralevaya-polosa/">Дюралевая полоса</a></li><li  class="submenu4" ><a href="dyuralevaya-truba/">Дюралевая труба</a></li><li  class="submenu4" ><a href="dyuralevyij-kvadrat/">Дюралевый квадрат</a></li><li  class="submenu4" ><a href="dyuralevyij-shveller/">Дюралевый швеллер</a></li><li  class="submenu4" ><a href="dyuralevaya-zagotovka-(bolvanka)/">Дюралевая заготовка (болванка)</a></li><li  class="submenu4" ><a href="dyuralevyij-riflyonyij-list/">Дюралевый рифлёный лист</a></li><li  class="submenu4" ><a href="dyuralevaya-lenta/">Дюралевая лента</a></li><li  class="submenu4" ><a href="dyuralevyij-shestigrannik/">Дюралевый шестигранник</a></li><li  class="submenu4" ><a href="dyuralevaya-provoloka/">Дюралевая проволока</a></li><li  class="submenu4" ><a href="dyuralevyij-list/">Дюралевый лист</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="latun/" >Латунь
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="krug-latunnyij/">Круг латунный</a></li><li  class="submenu4" ><a href="latunnaya-chushka/">Латунная чушка</a></li><li  class="submenu4" ><a href="shestigrannik-latunnyij/">Шестигранник латунный</a></li><li  class="submenu4" ><a href="latunnaya-plastina/">Латунная пластина</a></li><li  class="submenu4" ><a href="latunnyij-ugolok/">Латунный уголок</a></li><li  class="submenu4" ><a href="latunnaya-vtulka/">Латунная втулка</a></li><li  class="submenu4" ><a href="lenta-latunnaya/">Лента латунная</a></li><li  class="submenu4" ><a href="folga-latunnaya/">Фольга латунная</a></li><li  class="submenu4" ><a href="truba-latunnaya/">Труба латунная</a></li><li  class="submenu4" ><a href="plita-latunnaya/">Плита латунная</a></li><li  class="submenu4" ><a href="latunnaya-profilnaya-truba/">Латунная профильная труба</a></li><li  class="submenu4" ><a href="prutok-latunnyij/">Пруток латунный</a></li><li  class="submenu4" ><a href="shina-latunnaya/">Шина латунная</a></li><li  class="submenu4" ><a href="latunnaya-trubka/">Латунная трубка</a></li><li  class="submenu4" ><a href="latunnyij-list/">Латунный лист</a></li><li  class="submenu4" ><a href="latunnaya-provoloka/">Латунная проволока</a></li><li  class="submenu4" ><a href="setka-latunnaya/">Сетка латунная</a></li><li  class="submenu4" ><a href="latunnyij-kvadrat/">Латунный квадрат</a></li><li  class="submenu4" ><a href="latunnyij-profil/">Латунный профиль</a></li><li  class="submenu4" ><a href="latunnaya-polosa/">Латунная полоса</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="med/" >Медь
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="mednaya-provoloka/">Медная проволока</a></li><li  class="submenu4" ><a href="mednaya-folga/">Медная фольга</a></li><li  class="submenu4" ><a href="mednyij-list/">Медный лист</a></li><li  class="submenu4" ><a href="shina-mednaya/">Шина медная</a></li><li  class="submenu4" ><a href="mednaya-vtulka/">Медная втулка</a></li><li  class="submenu4" ><a href="mednyij-kvadrat/">Медный квадрат</a></li><li  class="submenu4" ><a href="mednaya-setka/">Медная сетка</a></li><li  class="submenu4" ><a href="mednaya-plastina/">Медная пластина</a></li><li  class="submenu4" ><a href="mednyij-slitok/">Медный слиток</a></li><li  class="submenu4" ><a href="mednaya-katanka/">Медная катанка</a></li><li  class="submenu4" ><a href="polosa-mednaya/">Полоса медная</a></li><li  class="submenu4" ><a href="mednyij-prutok/">Медный пруток</a></li><li  class="submenu4" ><a href="mednaya-lenta/">Медная лента</a></li><li  class="submenu4" ><a href="mednaya-plita/">Медная плита</a></li><li  class="submenu4" ><a href="mednaya-truba/">Медная труба</a></li><li  class="submenu4" ><a href="mednyij-poroshok/">Медный порошок</a></li><li  class="submenu4" ><a href="mednaya-chushka/">Медная чушка</a></li><li  class="submenu4" ><a href="mednyij-krug/">Медный круг</a></li><li  class="submenu4" ><a href="mednaya-dyujmovaya-truba/">Медная дюймовая труба</a></li><li  class="submenu4" ><a href="shestigrannik-mednyij/">Шестигранник медный</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="svinczovyij-prokat/" >Свинцовый прокат
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="svinczovyij-list/">Свинцовый лист</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="titan/" >Титан
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="titanovyij-prutok/">Титановый пруток</a></li><li  class="submenu4" ><a href="titanovaya-provoloka/">Титановая проволока</a></li><li  class="submenu4" ><a href="titanovyij-krug/">Титановый круг</a></li><li  class="submenu4" ><a href="titanovyij-list/">Титановый лист</a></li><li  class="submenu4" ><a href="titanovaya-plita/">Титановая плита</a></li><li  class="submenu4" ><a href="titanovyij-kvadrat/">Титановый квадрат</a></li><li  class="submenu4" ><a href="titanovaya-polosa/">Титановая полоса</a></li></ul></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="truboprovodnaya-armatura/" >Трубопроводная арматура
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="gidrant-pozharnyij-podzemnyij/">Гидрант пожарный подземный</a></li><li  class="submenu3" ><a href="klapan-predoxranitelnyij/">Клапан предохранительный</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="klapanyi/" >Клапаны
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="klapan-otsechnoj/">Клапан отсечной</a></li><li  class="submenu4" ><a href="klapan-zapornyij/">Клапан запорный</a></li><li  class="submenu4" ><a href="reguliruyushhij-klapan/">Регулирующий клапан</a></li><li  class="submenu4" ><a href="obratnyij-klapan/">Обратный клапан</a></li></ul></li><li  class="submenu3" ><a href="oporno-napravlyayushhee-kolczo/">Опорно направляющее кольцо</a></li><li  class="submenu3" ><a href="perexod-stalnoj/">Переход стальной</a></li><li  class="submenu3" ><a href="regulyator-davleniya/">Регулятор давления</a></li><li  class="submenu3" ><a href="termostaticheskij-klapan/">Термостатический клапан</a></li><li  class="submenu3" ><a href="truboprovodnyij-kompensator/">Трубопроводный компенсатор</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="chugunnaya-armatura/" >Чугунная арматура
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="dozhdepriemnik-chugunnyij/">Дождеприемник чугунный</a></li><li  class="submenu4" ><a href="lyuk-chugunnyij/">Люк чугунный</a></li><li  class="submenu4" ><a href="zadvizhka-chugunnaya/">Задвижка чугунная</a></li><li  class="submenu4" ><a href="trap-chugunnyij/">Трап чугунный</a></li><li  class="submenu4" ><a href="chugunnoe-koleno/">Чугунное колено</a></li><li  class="submenu4" ><a href="chugunnyij-sifon/">Чугунный сифон</a></li><li  class="submenu4" ><a href="chugunnaya-krestovina/">Чугунная крестовина</a></li><li  class="submenu4" ><a href="zatvor-chugunnyij/">Затвор чугунный</a></li><li  class="submenu4" ><a href="otvod-chugunnyij/">Отвод чугунный</a></li></ul></li><li  class="submenu3" ><a href="shibernaya-zadvizhka/">Шиберная задвижка</a></li><li  class="submenu3" ><a href="shtuczer/">Штуцер</a></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="stroitelnyie-materialyi/" >Строительные материалы
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="doloto/">Долото</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="zhelezobetonnyie-materialyi/" >Железобетонные материалы
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="izdeliya-dlya-energeticheskoj-otrasli/">Изделия для энергетической отрасли</a></li><li  class="submenu4" ><a href="izdeliya-dlya-dorozhnogo-stroitelstva/">Изделия для дорожного строительства</a></li><li  class="submenu4" ><a href="">Изделия для инженерных сетей</a></li><li  class="submenu4" ><a href="">Изделия для промышленного и гражданского строительства</a></li></ul></li><li  class="submenu3" ><a href="kozhux-ograditelnyij/">Кожух оградительный</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="kompoziczionnyie-materialyi/" >Композиционные материалы
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="kompozitnaya-armatura/">Композитная арматура</a></li></ul></li><li  class="submenu3" ><a href="metallocherepicza/">Металлочерепица</a></li><li  class="submenu3" ><a href="stroitelnyie-ograzhdeniya/">Строительные ограждения</a></li><li  class="submenu3" ><a href="sendvich-paneli/">Сэндвич-панели</a></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="speczialnyie-stali-i-splavyi/" >Специальные стали и сплавы
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="shestigrannik/">Шестигранник</a></li><li  class="submenu3" ><a href="ugolok/">Уголок</a></li><li  class="submenu3" ><a href="truba/">Труба</a></li><li  class="submenu3" ><a href="preczizionnyie-splavyi/">Прецизионные сплавы</a></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="svarochnyie-materialyi/" >Сварочные материалы
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="elektrodyi/">Электроды</a></li><li  class="submenu3" ><a href="elektrodnaya-lenta/">Электродная лента</a></li><li  class="submenu3" ><a href="flyus/">Флюс</a></li><li  class="submenu3" ><a href="svarochnaya-provoloka/">Сварочная проволока</a></li><li  class="submenu3" ><a href="prutok-dlya-naplavki/">Пруток для наплавки</a></li><li  class="submenu3" ><a href="prisadochnyij-prutok/">Присадочный пруток</a></li><li  class="submenu3" ><a href="prisadochnaya-provoloka/">Присадочная проволока</a></li><li  class="submenu3" ><a href="pripoi/">Припои</a></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="provoda,-kabelnaya-produkcziya/" >Провода, кабельная продукция
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="vzryivnoj-provod/">Взрывной провод</a></li><li  class="submenu3" ><a href="kabel-kontrolnyij/">Кабель контрольный</a></li><li  class="submenu3" ><a href="kabel-liftovoj/">Кабель лифтовой</a></li><li  class="submenu3" ><a href="kabel-nagrevatelnyij/">Кабель нагревательный</a></li><li  class="submenu3" ><a href="kabel-svarochnyij/">Кабель сварочный</a></li><li  class="submenu3" ><a href="kabel-svyazi/">Кабель связи</a></li><li  class="submenu3" ><a href="kabel-signalizaczii/">Кабель сигнализации</a></li><li  class="submenu3" ><a href="kabel-silovoj-gibkij/">Кабель силовой гибкий</a></li><li  class="submenu3" ><a href="kabel-shaxtnyij/">Кабель шахтный</a></li><li  class="submenu3" ><a href="montazhnyij-provod/">Монтажный провод</a></li><li  class="submenu3" ><a href="provod-bortovoj/">Провод бортовой</a></li><li  class="submenu3" ><a href="provod-obmotochnyij/">Провод обмоточный</a></li><li  class="submenu3" ><a href="provod-termostojkij/">Провод термостойкий</a></li><li  class="submenu3" ><a href="provod-upravleniya/">Провод управления</a></li><li  class="submenu3" ><a href="silovoj-kabel/">Силовой кабель</a></li><li  class="submenu3" ><a href="sudovoj-kabel/">Судовой кабель</a></li><li  class="submenu3" ><a href="termoelektrodnyij-provod/">Термоэлектродный провод</a></li></ul></li><li  class="submenu2 dropdown">
                                <a class="dropdown-toggle" href="nerzhaveyushhaya-stal/" >Нержавеющая сталь
                                    <span class="caret"></span>
                                </a><ul class="dropdown-menu"><li  class="submenu3" ><a href="zagotovka-nerzhaveyushhaya/">Заготовка нержавеющая</a></li><li  class="submenu3" ><a href="kvadrat-nerzhaveyushhij/">Квадрат нержавеющий</a></li><li  class="submenu3" ><a href="lenta-nerzhaveyushhaya/">Лента нержавеющая</a></li><li  class="submenu3" ><a href="list-nerzhaveyushhij/">Лист нержавеющий</a></li><li  class="submenu3" ><a href="nerzhaveyushhaya-provoloka/">Нержавеющая проволока</a></li><li  class="submenu3" ><a href="nerzhaveyushhaya-truba/">Нержавеющая труба</a></li><li  class="submenu3" ><a href="nerzhaveyushhij-krug/">Нержавеющий круг</a></li><li  class="submenu3" ><a href="plita-nerzhaveyushhaya/">Плита нержавеющая</a></li><li  class="submenu3" ><a href="polosa-nerzhaveyushhaya/">Полоса нержавеющая</a></li><li  class="submenu3" ><a href="prutok-nerzhaveyushhij/">Пруток нержавеющий</a></li><li  class="submenu3" ><a href="rulon-nerzhaveyushhij/">Рулон нержавеющий</a></li><li  class="submenu3" ><a href="setka-nerzhaveyushhaya/">Сетка нержавеющая</a></li><li  class="submenu3" ><a href="tros-nerzhaveyushhij/">Трос нержавеющий</a></li><li  class="submenu3" ><a href="trubka-kapillyarnaya-nerzhaveyushhaya/">Трубка капиллярная нержавеющая</a></li><li  class="submenu3" ><a href="ugolok-nerzhaveyushhij/">Уголок нержавеющий</a></li><li  class="submenu3" ><a href="shveller-nerzhaveyushhij/">Швеллер нержавеющий</a></li><li  class="submenu3" ><a href="shestigrannik-nerzhaveyushhij/">Шестигранник нержавеющий</a></li><li  class="submenu3" ><a href="shponochnyij-prokat-nerzhaveyushhij,-shponki-nerzhaveyushhie/">Шпоночный прокат нержавеющий, шпонки нержавеющие</a></li><li  class="submenu3" ><a href="shtrips-nerzhaveyushhij/">Штрипс нержавеющий</a></li><li  class="submenu3" ><a href="metallurgicheskoe-syiryo/">Металлургическое сырьё</a></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="ferrosplavyi/" >Ферросплавы
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="ferromarganecz/">Ферромарганец</a></li><li  class="submenu4" ><a href="ferrovolfram/">Ферровольфрам</a></li><li  class="submenu4" ><a href="ferrotitan/">Ферротитан</a></li><li  class="submenu4" ><a href="ferromolibden/">Ферромолибден</a></li><li  class="submenu4" ><a href="ferrobor/">Ферробор</a></li><li  class="submenu4" ><a href="ferrofosfor/">Феррофосфор</a></li><li  class="submenu4" ><a href="ferrovanadij/">Феррованадий</a></li><li  class="submenu4" ><a href="ferrosiliczij/">Ферросилиций</a></li><li  class="submenu4" ><a href="ferroczerij/">Ферроцерий</a></li><li  class="submenu4" ><a href="ferrosera/">Ферросера</a></li><li  class="submenu4" ><a href="silikokalczij/">Силикокальций</a></li><li  class="submenu4" ><a href="silikomarganecz/">Силикомарганец</a></li><li  class="submenu4" ><a href="mishmetall/">Мишметалл</a></li><li  class="submenu4" ><a href="ferroniobij/">Феррониобий</a></li><li  class="submenu4" ><a href="ferrosilikomarganecz/">Ферросиликомарганец</a></li><li  class="submenu4" ><a href="ferrosilikotitan/">Ферросиликотитан</a></li><li  class="submenu4" ><a href="ferroalyuminij/">Ферроалюминий</a></li><li  class="submenu4" ><a href="ferrosilikoczirkonij/">Ферросиликоцирконий</a></li><li  class="submenu4" ><a href="ferrosilikoxrom/">Ферросиликохром</a></li><li  class="submenu4" ><a href="ferronikel/">Ферроникель</a></li><li  class="submenu4" ><a href="ferrosilikovanadij/">Ферросиликованадий</a></li><li  class="submenu4" ><a href="ferroalyumoniobij/">Ферроалюмониобий</a></li><li  class="submenu4" ><a href="ferroxrom/">Феррохром</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="izdeliya-dlya-inzhenernyix-setej/" >Изделия для инженерных сетей
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="plityi-lotkov-kanalov/">Плиты лотков каналов</a></li><li  class="submenu4" ><a href="lotki-teplotrass/">Лотки теплотрасс</a></li><li  class="submenu4" ><a href="kryishki-i-plityi-perekryitiya-kolodczev/">Крышки и плиты перекрытия колодцев</a></li><li  class="submenu4" ><a href="plityi-dnishha-kolodczev/">Плиты днища колодцев</a></li><li  class="submenu4" ><a href="plityi-lotkov-teplotrass/">Плиты лотков теплотрасс</a></li><li  class="submenu4" ><a href="kolcza-kolodczev/">Кольца колодцев</a></li><li  class="submenu4" ><a href="lotki-kanalov/">Лотки каналов</a></li></ul></li><li  class="submenu3 dropdown">
                                        <a class="dropdown-toggle" href="izdeliya-dlya-promyishlennogo-i-grazhdanskogo-stroitelstva/" >Изделия для промышленного и гражданского строительства
                                            <span class="caret"></span>
                                        </a><ul class="dropdown-menu"><li  class="submenu4" ><a href="plityi-pokryitij/">Плиты покрытий</a></li></ul></li></ul></li></ul></li></ul>
                <ul class=""><li class="first"><a href="chernyij-metalloprokat/" >Черный металлопрокат</a></li><li class="active"><a href="czvetnoj-metalloprokat/" >Цветной металлопрокат</a></li><li class="last"><a href="truboprovodnaya-armatura/" >Трубопроводная арматура</a></li></ul>
            </div>

            <div class="search">
                <a href="#" class="search-btn"></a>
                <div class="search-form">
                    <form data-key="c53205c27441c77af8f65ac10665e2c97a3b1818" name="" class="msearch2" id="mse2_form" action="rezultaty-poiska.html" method="get">
                        <input type="text" class="text" placeholder="Поиск" name="query" value="">
                        <button type="submit" class="button"></button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- END navigation -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        (function($){
            $(document).ready(function(){
                let li = document.querySelectorAll('.mobile-catalog .nav li.dropdown');
                for(let i = 0; i < li.length; i++) {
                    li[i].querySelector('.dropdown-arrow').addEventListener('click', function() {
                        li[i].querySelector('.dropdown-row').classList.toggle('drop');
                    })
                }
                let submen = document.querySelectorAll('div.submenu2')
                for(let i = 0; i < submen.length; i++) {
                    submen[i].addEventListener('click', function() {
                        submen[i].parentElement.parentElement.querySelector('.dropdown-row.drop').classList.remove('drop');
                    })
                }
            });
            /*
            $(document).ready(function(){
            $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
            });
             });
            */
        })(jQuery);
    </script>

    <script>
        let nav = document.querySelector(".mobile-catalog");
        let header = document.querySelector('.mobmenu-toggle');
        header.addEventListener('click', function () {
            let data = document.querySelector('div[dir="ltr"]');
            if (data.classList.contains('hide-header')) {
                data.classList.remove('hide-header');
            } else {
                data.classList.toggle('hide-header');
            }
        })

        let catalog = document.querySelector('div.catalog');
        let mob = document.querySelector('.mobile-catalog');
        let sub = document.querySelectorAll('.mobile-catalog__sub');
        catalog.addEventListener('click', function () {
            let data = document.querySelector('div[dir="ltr"]');
            if (data.classList.contains('hide-subheader')) {
                data.classList.remove('hide-subheader');
            } else {
                data.classList.toggle('hide-subheader');
            }
            if (mob.classList.contains('show-mob')) {
                mob.classList.remove('show-mob');
                document.querySelector('body').style.overflow = 'auto'
            } else {
                mob.classList.toggle('show-mob');
                document.querySelector('body').style.overflow = 'hidden'
            }
            if (nav.classList.contains('show-nav')) {
                nav.classList.remove('show-nav');
                setTimeout(function() {
                    nav.classList.remove('subs');
                }, 500)
            }
            for(let i = 0; i < sub.length; i++) {
                if (sub[i].classList.contains('show-mob')) {
                    sub[i].classList.toggle('hide-mob');
                    sub[i].classList.remove('show-mob');
                    if (nav.classList.contains('show-nav')) {
                        sub[i].classList.toggle('toRight');
                    }
                } else {
                    sub[i].classList.remove('hide-mob');
                    sub[i].classList.toggle('show-mob');
                }
            }

        })

        let city = document.querySelector('a[data-target="#cfCity"]')
        city.addEventListener('click', function () {
            let headerTop = document.querySelector('header')
            let dir = document.querySelector('div[dir="ltr"]')
            headerTop.classList.toggle('city')
            dir.classList.toggle('city')
            let dismiss = document.querySelector('button[data-dismiss="modal"]');
            dismiss.addEventListener('click', function () {
                headerTop.classList.remove('city')
                dir.classList.remove('city')
            })
            let cfCity = document.querySelector('#cfCity');
            cfCity.addEventListener('click', function () {
                headerTop.classList.remove('city')
                dir.classList.remove('city')
            })
        })

        let closeCatalog = document.querySelector('.mobile-catalog div.submenu1 svg');
        closeCatalog.addEventListener('click', function () {
            if (mob.classList.contains('show-mob')) {
                mob.classList.remove('show-mob');
                document.querySelector('body').style.overflow = 'auto'
                for(let i = 0; i < sub.length; i++) {
                    sub[i].classList.remove('show-mob');
                    sub[i].classList.toggle('hide-mob');
                }
            }
        })
    </script>
    <div class="@yield('main_page_css_class')">
        <div class="container">
            <ol class="breadcrumb">
            @yield('breadcrumb_items')
            </ol>

            @yield('content')

        </div>
    </div>
    <!-- BEGIN footer -->
    <footer itemscope itemtype="http://schema.org/WPFooter" class="footer">
        <div class="footer-contacts" itemscope itemtype="http://schema.org/Place">
            <div class="container">
                <div class="content">
                    <div class="image">
                        <img src="{{ asset('images/footer-contacts.png') }}" alt="компания ООО «Фатом»">
                    </div>
                    <div class="address">
                        <p class="icon" itemprop="streetAddress">ул. Новороссийская,<br>
                            д. 236/1 лит. А, оф. 103</p>
                    </div>
                    <div class="mail">
                        <p class="icon"><a href="mailto:zakaz@ooofatom.ru ">zakaz@ooofatom.ru </a></p>
                    </div>
                    <div class="phone">
                        <a href="tel:88612052698" class="mgo-number" itemprop="telephone">8 (861) 205-26-98</a>
                    </div>
                    <div class="callback">
                        <a href="#" data-toggle="modal" data-target=".modal-callback">Заказать звонок</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-navigation">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-4 col-lg-3">
                        <div class="menu menu-main">
                            <div class="menu-heading">
                                Меню
                            </div>
                            <div class="menu-list">
                                <ul class=""><li class="first"><a href="" >Главная</a></li><li class="active"><a href="katalog-produkcii/" >Каталог</a></li><li><a href="uslugi/" >Услуги</a></li><li><a href="o-kompanii.html" >О компании</a></li><li><a href="dostavka-avtotransportom.html" >Доставка</a></li><li class="last"><a href="kontakty.html" >Контакты</a></li></ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-md-8 col-lg-9">
                        <div class="menu menu-catalog">
                            <div class="menu-heading">
                                Каталог
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="heading" itemprop="name">
                                        Черный металлопрокат
                                    </div>
                                    <div class="menu-list">

                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="heading" itemprop="name">
                                        Цветной металлопрокат
                                    </div>
                                    <div class="menu-list">

                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="heading" itemprop="name">
                                        Нержавеющий металлопрокат
                                    </div>
                                    <div class="menu-list">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-info">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="copy">
                            © 2021 - Фатом
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="politics">
                            <a href="politika-konfidencialnosti.html">Политика конфиденциальности</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END footer -->
</div>
<!-- END out -->

<a href="#out" class="arrow-up scroll"></a>

<!-- BEGIN modal-callback -->
<div class="modal modal-callback fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-header">
                <div class="modal-title">Заказать звонок</div>
            </div>
            <div class="modal-body">
                <form class="callback_form ajax_form" method="post" action="kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" name="callbackform" enctype="multipart/form-data">
                    <input type="hidden" name="pagename" value="Алюминиевая катанка 9,5 мм А5Е в Краснодаре">
                    <input type="hidden" name="pageurl" value="https://ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html">
                    <div class="form-group">
                        <input type="text" class="form-control" id="userNameCB" name="userNameCB" value="" placeholder="Имя *">
                        <span class="error_userNameCB"></span>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" id="userPhoneCB" name="userPhoneCB" value="" placeholder="Телефон *">
                        <span class="error_userPhoneCB"></span>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="userTextCB" name="userTextCB" value="" placeholder="Текст заявки"></textarea>
                    </div>
                    <input type="text" name="last_name" class="last_name" value="">
                    <input type="text" name="workemail" class="workemail" value="">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="submit" name="submitCB" id="submitCB" class="btn btn-primary">Отправить</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-file">
                                <a href="#" class="selectlink" for="uploadCB">Прикрепить файл <i></i></a>
                                <input type="file" name="uploadCB" id="file" onchange="getName(this);">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-personal">
                                    Нажимая кнопку «Отправить», я даю свое согласие на обработку моих <a href="politika-konfidencialnosti.html">персональных данных</a>
                                </div>
                            </div>
                        </div>
                    </div>



                    <input type="hidden" name="af_action" value="727504469bfebe8e50f0eee906c608a5" />
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END modal-callback -->

<!-- BEGIN quick_buy -->
<div class="modal quick_buy fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-header">
                <div class="modal-title">Заказ в один клик</div>
            </div>
            <div class="modal-body">
                <div class="quick_buy" id="quick_buy">
                    <form class="callback_form ajax_form af_quick_buy quick_buy-form" method="post" enctype="multipart/form-data">
                        <input type="hidden" class="qb-input" name="workemail">
                        <input type="hidden" class="qb-input" name="pagetitle" value="Купить Алюминиевая катанка 9,5 мм А5Е в Краснодаре ⭐ — ООО ФАТОМ">
                        <input type="hidden" class="qb-input" name="product_id" value="189156">
                        <input type="hidden" class="qb-input" name="price" value="208">
                        <input type="hidden" class="qb-input" name="url_page" value="https://ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html">

                        <div class="form-group">
                            <input type="text" class="qb-input form-control" placeholder="Имя *" name="receiver">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="qb-input form-control" placeholder="Телефон *" name="phone">
                        </div>


                        <div class="quick_buy-info">
                            <p>Заполните форму и наши менеджеры свяжутся с вами в течение часа.</p>
                            <br/>
                            <button type="submit" class="button btn btn-primary">Заказать</button>
                        </div>

                        <input type="hidden" name="af_action" value="d42462e1126579e3039f3437916a3f26" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END modal-callback -->




<!-- MODAL REGIONS -->
<div class="modal fade" id="cfCity" tabindex="-1" role="dialog" aria-labelledby="cfCityLabel">
    <div class="modal-dialog mcity" role="document">
        <div class="modal-content mcity">
            <div class="modal-header mcity">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title mcity" id="cfCityLabel">Выберите город</div>
            </div>
            <div class="modal-body mcity">
                <div class="form-horizontal">
                    <div class="form-group">
                        <input type="text" name="query" placeholder="Введите название" class="form-control" id="cfCitySearch" />
                    </div>
                    <div class="text-danger" id="cfCityError">По данному запросу ни одного города не найдено!</div>
                </div>
                <ul class="list-unstyled cfcity_list">
                    <li><a href="https://rnd.ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="1">Ростов-на-Дону</a></li><li><a href="https://stavropol.ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="2">Ставрополь</a></li><li><a href="https://ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="3">Краснодар</a></li><li><a href="https://sevastopol.ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="4">Севастополь</a></li><li><a href="https://sochi.ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="5">Сочи</a></li><li><a href="https://simferopol.ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" data-city="6">Симферополь</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL REGIONS -->

<!-- BEGIN modal-order -->
<div class="modal modal-order fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-header">
                <div class="modal-title">Сделать заказ</div>
            </div>
            <div class="modal-body">
                <div class="product-name"></div>
                <form class="order_form ajax_form" method="post" action="kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html" name="orderform" enctype="multipart/form-data">
                    <input type="hidden" name="pagename" value="Купить Алюминиевая катанка 9,5 мм А5Е в Краснодаре ⭐ — ООО ФАТОМ">
                    <input type="hidden" name="pageurl" value="https://ooofatom.ru/kupit-alyuminievaya-katanka-9,5-mm-a5e-v-⭐-—-ooo-fatom.html">
                    <input type="text" class="form-control hidden product" hidden name="product" id="">
                    <div class="form-group">
                        <input type="text" class="form-control" id="userNameOR" name="userNameOR" value="" placeholder="Имя *">
                        <span class="error_userNameOR"></span>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" id="userPhoneOR" name="userPhoneOR" value="" placeholder="Телефон *">
                        <span class="error_userPhoneOR"></span>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="userMailOR" name="userMailOR" value="" placeholder="E-mail">
                        <span class="error_userMailOR"></span>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="userTextOR" name="userTextOR" value="" placeholder="Текст заявки"></textarea>
                    </div>
                    <input type="text" name="last_name" class="last_name" value="">
                    <input type="text" name="workemail" class="workemail" value="">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="submit" name="submitOR" id="submitOR" class="btn btn-primary">Отправить</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-personal">
                                    Нажимая кнопку «Отправить», я даю свое согласие на обработку моих <a href="politika-konfidencialnosti.html">персональных данных</a>
                                </div>
                            </div>
                        </div>
                    </div>



                    <input type="hidden" name="af_action" value="7fb050b2d8debba664a47fb4f5a2878a" />
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END modal-order -->

<!-- BEGIN cookie -->
<div class="cookie">
    <div class="container">
        <div class="cookie-content">
            <div class="text">
                Используя наш сайт, вы принимаете условия, согласно которым мы используем cookie-файлы для анализа данных и создания контента (в том числе и рекламного) на основе ваших интересов. Узнайте больше в разделе политика cookie.
            </div>
            <div class="button">
                <a href="#" class="btn btn-primary">Я согласен</a>
            </div>
        </div>
    </div>
</div>
<!-- END modal-order -->

<div class="whatsapp">
    <div class="whatsapp__border"></div>
    <div class="whatsapp__content">
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"><path fill="#FFFFFF" d="M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z"/>
        </svg>
    </div>
    <div class="whatsapp__pulse"></div>
</div>

<!--[if IE]><script src="tpl/js/html5shiv.js"></script><![endif]-->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
<script src="{{ asset('js/slick/slick.min.js') }}"></script>
<script src="{{ asset('js/fancybox/fancybox.js') }}"></script>
<script src="{{ asset('js/inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script src="{{ asset('js/jquery-ui/jquery-ui.touch-punch.js') }}"></script>
<script src="{{ asset('js/mCustomScrollbar/mCustomScrollbar.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.js" type="text/javascript"></script>
<script>
    $("#phone").mask("+7(999)999-99-99");
</script>

<script>
    jQuery(($) => {
        // input type number
        // Уменьшаем на 1
        $(document).on('click', '.input-number__minus', function () {
            let total = $(this).next();
            if (total.val() > 1) {
                total.val(+total.val() - 1);
            }
        });
        // Увеличиваем на 1
        $(document).on('click', '.input-number__plus', function () {
            let total = $(this).prev();
            total.val(+total.val() + 1);
        });
        // Запрещаем ввод текста
        document.querySelectorAll('.input-number__input').forEach(function (el) {
            el.addEventListener('input', function () {
                this.value = this.value.replace(/[^\d]/g, '');
            });
        });
    });
</script>

{{--<script type="text/javascript" src="{{ asset('js/ajaxform/default.js') }}""></script>
<script type="text/javascript">AjaxForm.initialize({"assetsUrl":"\/assets\/components\/ajaxform\/","actionUrl":"\/assets\/components\/ajaxform\/action.php","closeMessage":"\u0437\u0430\u043a\u0440\u044b\u0442\u044c \u0432\u0441\u0435","formSelector":"form.ajax_form","pageId":189156});</script>
<script type="text/javascript">cityFields = {actionUrl: "/assets/components/cityfields/action.php",cityInDomain: true,mainHost: "ooofatom.ru"};typeof jQuery == "function" || document.write("<script type=\"text/javascript\" src=\"/assets/components/cityfields/js/web/jquery.min.js\"><\/script>");</script>
<script type="text/javascript" src="{{ asset('js/cityfields/cityselect.js') }}></script>--}}
</body>
</html>
